# Enzian App DCS.


## Simple integration of ECI with DCS.

* Instantiates 2 memory controllers, configured for 16GB RDIMMs, two channels, 1st and 4th.
* Connects ECI link with 2 instances of directory controllers, one for odd CL indices and other for even CL indices.
* Contains scripts for two projects.
  * **DCS Single DCU:** Project with a DCS containing a single DCU (mainly for testing).
  * **DCS:** Project with a DCS containing all required DCUs.


## Folders Desc:

* **enzian_eci_toolkit:** Submodule containing ECI components.
* **enzian-optimized-dcu:** Submodule containing DCS components.
* **hdl:** Top level that integrates ECI and DCS.
* **xdc:** Constraints specific to this App.

## TCL Scripts:
* **DCS Single DCU**
  * create_dcs1dcu_project.tcl: Create DCS single DCU project. 
  * synth_dcs1dcu_project.tcl: Synthesize DCS single DCU project and checkpoint.
  * impl_shell_dcs1dcu.tcl: Links synthesized shell checkpoint with synthesized app checkpoint, implements it and creates a checkpoint.
  * wrbit_dcs1dcu.tcl: write bitstream 
* **DCS**
  * create_enzian_app_dcs_project.tcl: Create DCS project.
  * synth_enzian_app_dcs_project.tcl: Synthesize DCS project and checkpoint.
  * impl_enzian_app_dcs_project.tcl: Links synthesized shell checkpoint with synthesized app checkpoint, implements it and creates a checkpoint.
  * wrbit_enzian_app_dcs_project.tcl: Write bitstream from implemented checkpoint.


## Steps immediately after cloning the repository

* cd to repo
* you will find that the ECI, DCS, submodule directories would be empty.
* **git submodule update --init**
  * This will initialize the ECI and DCS submodules.


## Steps to generate bitstream.
* **Synthesize Shell and checkpoint**
  * If you already have the synthesized shell checkpoint, you can skip this section.
  * Otherwise follow the steps in enzian shell repo to create a synthesis checkpoint.

* **Synthesize your app and checkpoint**
  * create an empty build_app directory (preferably **outside** the repository).
  * cd to build_app directory.
  * create the vivado project.
    * <path-to-Vivado> -source <path-to-sources>/create_sample_application_project.tcl -mode batch
  * build (synthesize, implement and write a bitstream) the created project
    * <path-to-Vivado> -source <path-to-static-shell>/build_app.tcl -mode batch

## To run on Enzian
  * Refer to the Enzian quick start guide.
