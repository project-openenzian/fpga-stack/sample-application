# Standalone build

The scripts build a standalone version, without a pre-built shell. The Enzian shell is checked out, built and then the application is built.

To build it, just execute the script `build.sh` (takes 5-6h or `build_fast.sh` to get a worse implemented version in 2-3h).

When it finishes, a directory `build_sample_application` is created. It containts a bitstream `shell_enzian_app_dcs.bit` that can be used to test the Enzian platform out.
