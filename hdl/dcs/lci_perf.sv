/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-12-01
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef LCI_PERF_SV
`define LCI_PERF_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

module lci_perf #
  (
   // 0 for LCI latency measurement,
   // 1 for LCI tput measurement.
   parameter LCI_LAT_TPUT = 0,
   parameter NUM_REQS_TO_WAIT = 65536,
   parameter NUM_ADDR_TO_ISSUE = 65536,
   parameter PERF_REGS_WIDTH = 32,
   parameter FIFO_DEPTH = 512
   )
   (
    input logic 				 clk,
    input logic 				 reset,
    // Wait for number of requests.
    input logic 				 odd_wait_req_valid_i,
    input logic 				 even_wait_req_valid_i,

    // Output LCI VC 16. odd cl index.
    output logic [ECI_WORD_WIDTH-1:0] 		 lci_odd_idx_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lci_odd_idx_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lci_odd_idx_vc_o,
    output logic 				 lci_odd_idx_valid_o,
    input logic 				 lci_odd_idx_ready_i,
    // Input LCIA VC 18. odd cl index.
    input logic [ECI_WORD_WIDTH-1:0] 		 lcia_odd_idx_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcia_odd_idx_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  lcia_odd_idx_vc_i,
    input logic 				 lcia_odd_idx_valid_i,
    output logic 				 lcia_odd_idx_ready_o,
    // Output unlock VC 18. odd cl index.
    output logic [ECI_WORD_WIDTH-1:0] 		 ul_odd_idx_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 ul_odd_idx_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] ul_odd_idx_vc_o,
    output logic 				 ul_odd_idx_valid_o,
    input logic 				 ul_odd_idx_ready_i,

    // Output LCI VC 17. even cl index.
    output logic [ECI_WORD_WIDTH-1:0] 		 lci_even_idx_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lci_even_idx_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lci_even_idx_vc_o,
    output logic 				 lci_even_idx_valid_o,
    input logic 				 lci_even_idx_ready_i,
    // Input LCIA VC 19. even cl index.
    input logic [ECI_WORD_WIDTH-1:0] 		 lcia_even_idx_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcia_even_idx_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  lcia_even_idx_vc_i,
    input logic 				 lcia_even_idx_valid_i,
    output logic 				 lcia_even_idx_ready_o,
    // Output unlock VC 19. even cl index.
    output logic [ECI_WORD_WIDTH-1:0] 		 ul_even_idx_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 ul_even_idx_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] ul_even_idx_vc_o,
    output logic 				 ul_even_idx_valid_o,
    input logic 				 ul_even_idx_ready_i,
    // once all addresses are issued. 
    output logic 				 done_o
    );

   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]        hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
   } eci_hdr_if_t;

   typedef struct packed {
      logic [DS_ADDR_WIDTH-1:0] addr;
      logic 			valid;
      logic 			ready;
   } cl_addr_if_t;

   // perf gen seq aliased signals. 
   cl_addr_if_t seq_odd_o, seq_even_o;
   logic 			seq_odd_wait_rsp_valid_i;
   logic 			seq_even_wait_rsp_valid_i;
   logic 			seq_done_o;
   logic [PERF_REGS_WIDTH-1:0] 	perf_cyc_st_to_done_o;
   logic [PERF_REGS_WIDTH-1:0] 	perf_num_tr_done_o;

   cl_addr_if_t lci_odd_addr_i;
   eci_hdr_if_t lci_odd_hdr_o, lcia_odd_hdr_i, ul_odd_hdr_o;
   logic 			odd_tr_fin_valid_o;
   logic [PERF_REGS_WIDTH-1:0] 	odd_lat_data_o;
   logic 			odd_lat_data_valid_o;
   logic [PERF_REGS_WIDTH-1:0] 	odd_min_lat_o;
   logic [PERF_REGS_WIDTH-1:0] 	odd_max_lat_o;

   cl_addr_if_t lci_even_addr_i;
   eci_hdr_if_t lci_even_hdr_o, lcia_even_hdr_i, ul_even_hdr_o;
   logic 			even_tr_fin_valid_o;
   logic [PERF_REGS_WIDTH-1:0] 	even_lat_data_o;
   logic 			even_lat_data_valid_o;
   logic [PERF_REGS_WIDTH-1:0] 	even_min_lat_o;
   logic [PERF_REGS_WIDTH-1:0] 	even_max_lat_o;
   

   always_comb begin : OUT_ASSIGN
      lci_odd_idx_hdr_o		= lci_odd_hdr_o.hdr;
      lci_odd_idx_size_o	= lci_odd_hdr_o.size;
      lci_odd_idx_vc_o		= lci_odd_hdr_o.vc;
      lci_odd_idx_valid_o	= lci_odd_hdr_o.valid;
      lcia_odd_idx_ready_o	= lcia_odd_hdr_i.ready;
      ul_odd_idx_hdr_o		= ul_odd_hdr_o.hdr;
      ul_odd_idx_size_o		= ul_odd_hdr_o.size;
      ul_odd_idx_vc_o		= ul_odd_hdr_o.vc;
      ul_odd_idx_valid_o	= ul_odd_hdr_o.valid;
      lci_even_idx_hdr_o	= lci_even_hdr_o.hdr;
      lci_even_idx_size_o	= lci_even_hdr_o.size;
      lci_even_idx_vc_o		= lci_even_hdr_o.vc;
      lci_even_idx_valid_o	= lci_even_hdr_o.valid;
      lcia_even_idx_ready_o	= lcia_even_hdr_i.ready;
      ul_even_idx_hdr_o		= ul_even_hdr_o.hdr;
      ul_even_idx_size_o	= ul_even_hdr_o.size;
      ul_even_idx_vc_o		= ul_even_hdr_o.vc;
      ul_even_idx_valid_o	= ul_even_hdr_o.valid;
      done_o = seq_done_o;
   end : OUT_ASSIGN
   
   always_comb begin : SEQ_GEN_IP_ASSIGN
      seq_odd_o.ready		= lci_odd_addr_i.ready;
      seq_even_o.ready		= lci_even_addr_i.ready;
      seq_odd_wait_rsp_valid_i	= odd_tr_fin_valid_o;
      seq_even_wait_rsp_valid_i = even_tr_fin_valid_o;
   end : SEQ_GEN_IP_ASSIGN
   perf_gen_seq_aliased #
     (
      .NUM_REQS_TO_WAIT(NUM_REQS_TO_WAIT),
      .NUM_ADDR_TO_ISSUE(NUM_ADDR_TO_ISSUE),
      .PERF_REGS_WIDTH(PERF_REGS_WIDTH)
       )
   perf_seq_addr_gen
     (
      .clk			(clk),
      .reset			(reset),
      .odd_wait_req_valid_i	(odd_wait_req_valid_i),
      .even_wait_req_valid_i	(even_wait_req_valid_i),
      .odd_al_addr_o		(seq_odd_o.addr),
      .odd_al_addr_valid_o	(seq_odd_o.valid),
      .odd_al_addr_ready_i	(seq_odd_o.ready),
      .even_al_addr_o		(seq_even_o.addr),
      .even_al_addr_valid_o	(seq_even_o.valid),
      .even_al_addr_ready_i	(seq_even_o.ready),
      .odd_wait_rsp_valid_i     (seq_odd_wait_rsp_valid_i),
      .even_wait_rsp_valid_i	(seq_even_wait_rsp_valid_i),
      .done_o			(seq_done_o),
      .perf_cyc_st_to_done_o	(perf_cyc_st_to_done_o),
      .perf_num_tr_done_o	(perf_num_tr_done_o)
      );

   generate
      if(LCI_LAT_TPUT == 0) begin
	 
	 // Measuring the latency of LCI commands.
	 // Odd cl indices are sent here.
	 always_comb begin : ODD_IP_ASSIGN
	    lci_odd_addr_i.addr		= seq_odd_o.addr;
	    lci_odd_addr_i.valid	= seq_odd_o.valid;
	    lci_odd_hdr_o.ready		= lci_odd_idx_ready_i;
	    lcia_odd_hdr_i.hdr		= lcia_odd_idx_hdr_i;
	    lcia_odd_hdr_i.size		= lcia_odd_idx_size_i;
	    lcia_odd_hdr_i.vc		= lcia_odd_idx_vc_i;
	    lcia_odd_hdr_i.valid	= lcia_odd_idx_valid_i;
	    ul_odd_hdr_o.ready		= ul_odd_idx_ready_i;
	 end : ODD_IP_ASSIGN
	 cli_lat_load_gen #
	   (
	    .PERF_REGS_WIDTH(PERF_REGS_WIDTH),
	    .FIFO_DEPTH(FIFO_DEPTH)
	     )
	 odd_cli_m_inst
	   (
	    .clk			(clk),
	    .reset			(reset),
	    .al_addr_i		        (lci_odd_addr_i.addr),
	    .al_addr_valid_i	        (lci_odd_addr_i.valid),
	    .al_addr_ready_o	        (lci_odd_addr_i.ready),
	    .lcl_fwd_wod_hdr_o		(lci_odd_hdr_o.hdr),
	    .lcl_fwd_wod_pkt_size_o	(lci_odd_hdr_o.size),
	    .lcl_fwd_wod_pkt_vc_o	(lci_odd_hdr_o.vc),
	    .lcl_fwd_wod_pkt_valid_o	(lci_odd_hdr_o.valid),
	    .lcl_fwd_wod_pkt_ready_i	(lci_odd_hdr_o.ready),
	    .lcl_rsp_wod_hdr_i		(lcia_odd_hdr_i.hdr),
	    .lcl_rsp_wod_pkt_size_i	(lcia_odd_hdr_i.size),
	    .lcl_rsp_wod_pkt_vc_i	(lcia_odd_hdr_i.vc),
	    .lcl_rsp_wod_pkt_valid_i	(lcia_odd_hdr_i.valid),
	    .lcl_rsp_wod_pkt_ready_o	(lcia_odd_hdr_i.ready),
	    .lcl_rsp_wod_hdr_o		(ul_odd_hdr_o.hdr),
	    .lcl_rsp_wod_pkt_size_o	(ul_odd_hdr_o.size),
	    .lcl_rsp_wod_pkt_vc_o	(ul_odd_hdr_o.vc),
	    .lcl_rsp_wod_pkt_valid_o	(ul_odd_hdr_o.valid),
	    .lcl_rsp_wod_pkt_ready_i	(ul_odd_hdr_o.ready),
	    .tr_fin_valid_o		(odd_tr_fin_valid_o),
	    .lat_data_o			(odd_lat_data_o),
	    .lat_data_valid_o		(odd_lat_data_valid_o),
	    .min_lat_o			(odd_min_lat_o),
	    .max_lat_o			(odd_max_lat_o)
	    );

	 // Even cl indices are sent here.
	 always_comb begin : EVEN_IP_ASSIGN
	    lci_even_addr_i.addr	= seq_even_o.addr;
	    lci_even_addr_i.valid	= seq_even_o.valid;
	    lci_even_hdr_o.ready	= lci_even_idx_ready_i;
	    lcia_even_hdr_i.hdr		= lcia_even_idx_hdr_i;
	    lcia_even_hdr_i.size	= lcia_even_idx_size_i;
	    lcia_even_hdr_i.vc		= lcia_even_idx_vc_i;
	    lcia_even_hdr_i.valid	= lcia_even_idx_valid_i;
	    ul_even_hdr_o.ready		= ul_even_idx_ready_i;
	 end : EVEN_IP_ASSIGN
	 cli_lat_load_gen #
	   (
	    .PERF_REGS_WIDTH(PERF_REGS_WIDTH),
	    .FIFO_DEPTH(FIFO_DEPTH)
	     )
	 even_cli_m_inst
	   (
	    .clk			(clk),
	    .reset			(reset),
	    .al_addr_i		        (lci_even_addr_i.addr),
	    .al_addr_valid_i	        (lci_even_addr_i.valid),
	    .al_addr_ready_o	        (lci_even_addr_i.ready),
	    .lcl_fwd_wod_hdr_o		(lci_even_hdr_o.hdr),
	    .lcl_fwd_wod_pkt_size_o	(lci_even_hdr_o.size),
	    .lcl_fwd_wod_pkt_vc_o	(lci_even_hdr_o.vc),
	    .lcl_fwd_wod_pkt_valid_o	(lci_even_hdr_o.valid),
	    .lcl_fwd_wod_pkt_ready_i	(lci_even_hdr_o.ready),
	    .lcl_rsp_wod_hdr_i		(lcia_even_hdr_i.hdr),
	    .lcl_rsp_wod_pkt_size_i	(lcia_even_hdr_i.size),
	    .lcl_rsp_wod_pkt_vc_i	(lcia_even_hdr_i.vc),
	    .lcl_rsp_wod_pkt_valid_i	(lcia_even_hdr_i.valid),
	    .lcl_rsp_wod_pkt_ready_o	(lcia_even_hdr_i.ready),
	    .lcl_rsp_wod_hdr_o		(ul_even_hdr_o.hdr),
	    .lcl_rsp_wod_pkt_size_o	(ul_even_hdr_o.size),
	    .lcl_rsp_wod_pkt_vc_o	(ul_even_hdr_o.vc),
	    .lcl_rsp_wod_pkt_valid_o	(ul_even_hdr_o.valid),
	    .lcl_rsp_wod_pkt_ready_i	(ul_even_hdr_o.ready),
	    .tr_fin_valid_o		(even_tr_fin_valid_o),
	    .lat_data_o			(even_lat_data_o),
	    .lat_data_valid_o		(even_lat_data_valid_o),
	    .min_lat_o			(even_min_lat_o),
	    .max_lat_o			(even_max_lat_o)
	    );
	 
      end else begin // if (LCI_LAT_TPUT == 0)
	 
	 // Measuring the throughput of LCI commands.
	 // Odd cl indices are sent here.
	 always_comb begin : ODD_IP_ASSIGN
	    lci_odd_addr_i.addr		= seq_odd_o.addr;
	    lci_odd_addr_i.valid	= seq_odd_o.valid;
	    lci_odd_hdr_o.ready		= lci_odd_idx_ready_i;
	    lcia_odd_hdr_i.hdr		= lcia_odd_idx_hdr_i;
	    lcia_odd_hdr_i.size		= lcia_odd_idx_size_i;
	    lcia_odd_hdr_i.vc		= lcia_odd_idx_vc_i;
	    lcia_odd_hdr_i.valid	= lcia_odd_idx_valid_i;
	    ul_odd_hdr_o.ready		= ul_odd_idx_ready_i;
	 end : ODD_IP_ASSIGN
	 cli_tput_load_gen #
	   (
	    .PERF_REGS_WIDTH(PERF_REGS_WIDTH)
	     )
	 odd_cli_m_inst
	   (
	    .clk			(clk),
	    .reset			(reset),
	    .al_addr_i		        (lci_odd_addr_i.addr),
	    .al_addr_valid_i	        (lci_odd_addr_i.valid),
	    .al_addr_ready_o	        (lci_odd_addr_i.ready),
	    .lcl_fwd_wod_hdr_o		(lci_odd_hdr_o.hdr),
	    .lcl_fwd_wod_pkt_size_o	(lci_odd_hdr_o.size),
	    .lcl_fwd_wod_pkt_vc_o	(lci_odd_hdr_o.vc),
	    .lcl_fwd_wod_pkt_valid_o	(lci_odd_hdr_o.valid),
	    .lcl_fwd_wod_pkt_ready_i	(lci_odd_hdr_o.ready),
	    .lcl_rsp_wod_hdr_i		(lcia_odd_hdr_i.hdr),
	    .lcl_rsp_wod_pkt_size_i	(lcia_odd_hdr_i.size),
	    .lcl_rsp_wod_pkt_vc_i	(lcia_odd_hdr_i.vc),
	    .lcl_rsp_wod_pkt_valid_i	(lcia_odd_hdr_i.valid),
	    .lcl_rsp_wod_pkt_ready_o	(lcia_odd_hdr_i.ready),
	    .lcl_rsp_wod_hdr_o		(ul_odd_hdr_o.hdr),
	    .lcl_rsp_wod_pkt_size_o	(ul_odd_hdr_o.size),
	    .lcl_rsp_wod_pkt_vc_o	(ul_odd_hdr_o.vc),
	    .lcl_rsp_wod_pkt_valid_o	(ul_odd_hdr_o.valid),
	    .lcl_rsp_wod_pkt_ready_i	(ul_odd_hdr_o.ready),
	    .tr_fin_valid_o		(odd_tr_fin_valid_o)
	    );

	 // Even cl indices are sent here.
	 always_comb begin : EVEN_IP_ASSIGN
	    lci_even_addr_i.addr	= seq_even_o.addr;
	    lci_even_addr_i.valid	= seq_even_o.valid;
	    lci_even_hdr_o.ready	= lci_even_idx_ready_i;
	    lcia_even_hdr_i.hdr		= lcia_even_idx_hdr_i;
	    lcia_even_hdr_i.size	= lcia_even_idx_size_i;
	    lcia_even_hdr_i.vc		= lcia_even_idx_vc_i;
	    lcia_even_hdr_i.valid	= lcia_even_idx_valid_i;
	    ul_even_hdr_o.ready		= ul_even_idx_ready_i;
	 end : EVEN_IP_ASSIGN
	 cli_tput_load_gen #
	   (
	    .PERF_REGS_WIDTH(PERF_REGS_WIDTH)
	     )
	 even_cli_m_inst
	   (
	    .clk			(clk),
	    .reset			(reset),
	    .al_addr_i		        (lci_even_addr_i.addr),
	    .al_addr_valid_i	        (lci_even_addr_i.valid),
	    .al_addr_ready_o	        (lci_even_addr_i.ready),
	    .lcl_fwd_wod_hdr_o		(lci_even_hdr_o.hdr),
	    .lcl_fwd_wod_pkt_size_o	(lci_even_hdr_o.size),
	    .lcl_fwd_wod_pkt_vc_o	(lci_even_hdr_o.vc),
	    .lcl_fwd_wod_pkt_valid_o	(lci_even_hdr_o.valid),
	    .lcl_fwd_wod_pkt_ready_i	(lci_even_hdr_o.ready),
	    .lcl_rsp_wod_hdr_i		(lcia_even_hdr_i.hdr),
	    .lcl_rsp_wod_pkt_size_i	(lcia_even_hdr_i.size),
	    .lcl_rsp_wod_pkt_vc_i	(lcia_even_hdr_i.vc),
	    .lcl_rsp_wod_pkt_valid_i	(lcia_even_hdr_i.valid),
	    .lcl_rsp_wod_pkt_ready_o	(lcia_even_hdr_i.ready),
	    .lcl_rsp_wod_hdr_o		(ul_even_hdr_o.hdr),
	    .lcl_rsp_wod_pkt_size_o	(ul_even_hdr_o.size),
	    .lcl_rsp_wod_pkt_vc_o	(ul_even_hdr_o.vc),
	    .lcl_rsp_wod_pkt_valid_o	(ul_even_hdr_o.valid),
	    .lcl_rsp_wod_pkt_ready_i	(ul_even_hdr_o.ready),
	    .tr_fin_valid_o		(even_tr_fin_valid_o)
	    );
	 // Assign default values for ILA.
	 always_comb begin : DEFAULT_ASSIGN
	    odd_lat_data_o		= '0;
	    odd_lat_data_valid_o	= '0;
	    odd_min_lat_o		= '1;
	    odd_max_lat_o		= '0;
	    even_lat_data_o	        = '0;
	    even_lat_data_valid_o	= '0;
	    even_min_lat_o		= '1;
	    even_max_lat_o		= '0;
	 end : DEFAULT_ASSIGN
      end // else: !if(LCI_LAT_TPUT == 0)
      
   endgenerate
   
   perf_ila
     perf_ila_inst
       (
   	.clk(clk),
   	.probe0(seq_done_o),		//1
   	.probe1(perf_cyc_st_to_done_o), //32
   	.probe2(perf_num_tr_done_o),	//32
   	.probe3(even_lat_data_o),	//32
   	.probe4(even_lat_data_valid_o), //1
   	.probe5(even_min_lat_o),	//32
   	.probe6(even_max_lat_o),	//32
   	.probe7(odd_lat_data_o),	//32
   	.probe8(odd_lat_data_valid_o),	//1
   	.probe9(odd_min_lat_o),		//32
   	.probe10(odd_max_lat_o)		//32
   	);
   
endmodule // lci_perf

`endif
