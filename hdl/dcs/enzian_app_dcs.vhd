-------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity enzian_app_dcs is
generic(
  DDR_OR_BRAM  : integer := 1; --0 BRAM, 1 DDR.
  LCI_PERF_GEN : integer := 1; --0 no LCI perf, 1 LCI perf
  LCI_LAT_TPUT : integer := 0  -- if LCI perf is 1, 0 is latency test, 1 is tput
                               -- test.
  );
port (
-- 322.265625 MHz
    clk_sys                 : in std_logic;
-- programmed to 100MHz
    prgc0_clk_p             : in std_logic;
    prgc0_clk_n             : in std_logic;
-- programmed to 300MHz
    prgc1_clk_p             : in std_logic;
    prgc1_clk_n             : in std_logic;
-- power on reset
    reset_sys               : in std_logic;
-- ECI link status
    link1_up                : in std_logic;
    link2_up                : in std_logic;
-- ECI links
    link1_in_data           : in std_logic_vector(447 downto 0);
    link1_in_vc_no          : in std_logic_vector(27 downto 0);
    link1_in_we2            : in std_logic_vector(6 downto 0);
    link1_in_we3            : in std_logic_vector(6 downto 0);
    link1_in_we4            : in std_logic_vector(6 downto 0);
    link1_in_we5            : in std_logic_vector(6 downto 0);
    link1_in_valid          : in std_logic;
    link1_in_credit_return  : out std_logic_vector(12 downto 2);

    link1_out_hi_data       : out std_logic_vector(575 downto 0);
    link1_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_hi_size       : out std_logic_vector(2 downto 0);
    link1_out_hi_valid      : out std_logic;
    link1_out_hi_ready      : in std_logic;

    link1_out_lo_data       : out std_logic_vector(63 downto 0);
    link1_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_lo_valid      : out std_logic;
    link1_out_lo_ready      : in std_logic;
    link1_out_credit_return : in std_logic_vector(12 downto 2);

    link2_in_data           : in std_logic_vector(447 downto 0);
    link2_in_vc_no          : in std_logic_vector(27 downto 0);
    link2_in_we2            : in std_logic_vector(6 downto 0);
    link2_in_we3            : in std_logic_vector(6 downto 0);
    link2_in_we4            : in std_logic_vector(6 downto 0);
    link2_in_we5            : in std_logic_vector(6 downto 0);
    link2_in_valid          : in std_logic;
    link2_in_credit_return  : out std_logic_vector(12 downto 2);

    link2_out_hi_data       : out std_logic_vector(575 downto 0);
    link2_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_hi_size       : out std_logic_vector(2 downto 0);
    link2_out_hi_valid      : out std_logic;
    link2_out_hi_ready      : in std_logic;

    link2_out_lo_data       : out std_logic_vector(63 downto 0);
    link2_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_lo_valid      : out std_logic;
    link2_out_lo_ready      : in std_logic;
    link2_out_credit_return : in std_logic_vector(12 downto 2);

    disable_2nd_link        : out std_logic;
-- AXI Lite FPGA -> CPU
    m_io_axil_awaddr        : out std_logic_vector(43 downto 0);
    m_io_axil_awvalid       : out std_logic;
    m_io_axil_awready       : in std_logic;
    m_io_axil_wdata         : out std_logic_vector(63 downto 0);
    m_io_axil_wstrb         : out std_logic_vector(7 downto 0);
    m_io_axil_wvalid        : out std_logic;
    m_io_axil_wready        : in std_logic;
    m_io_axil_bresp         : in std_logic_vector(1 downto 0);
    m_io_axil_bvalid        : in std_logic;
    m_io_axil_bready        : out std_logic;
    m_io_axil_araddr        : out std_logic_vector(43 downto 0);
    m_io_axil_arvalid       : out std_logic;
    m_io_axil_arready       : in  std_logic;
    m_io_axil_rdata         : in std_logic_vector(63 downto 0);
    m_io_axil_rresp         : in std_logic_vector(1 downto 0);
    m_io_axil_rvalid        : in std_logic;
    m_io_axil_rready        : out std_logic;
-- AXI Lite CPU -> FPGA
    s_io_axil_awaddr        : in std_logic_vector(43 downto 0);
    s_io_axil_awvalid       : in std_logic;
    s_io_axil_awready       : out std_logic;
    s_io_axil_wdata         : in std_logic_vector(63 downto 0);
    s_io_axil_wstrb         : in std_logic_vector(7 downto 0);
    s_io_axil_wvalid        : in std_logic;
    s_io_axil_wready        : out std_logic;
    s_io_axil_bresp         : out std_logic_vector(1 downto 0);
    s_io_axil_bvalid        : out std_logic;
    s_io_axil_bready        : in std_logic;
    s_io_axil_araddr        : in std_logic_vector(43 downto 0);
    s_io_axil_arvalid       : in std_logic;
    s_io_axil_arready       : out  std_logic;
    s_io_axil_rdata         : out std_logic_vector(63 downto 0);
    s_io_axil_rresp         : out std_logic_vector(1 downto 0);
    s_io_axil_rvalid        : out std_logic;
    s_io_axil_rready        : in std_logic;
-- BSCAN slave port for ILAs, VIOs, MIGs, MDMs etc.
    s_bscan_bscanid_en      : in std_logic;
    s_bscan_capture         : in std_logic;
    s_bscan_drck            : in std_logic;
    s_bscan_reset           : in std_logic;
    s_bscan_runtest         : in std_logic;
    s_bscan_sel             : in std_logic;
    s_bscan_shift           : in std_logic;
    s_bscan_tck             : in std_logic;
    s_bscan_tdi             : in std_logic;
    s_bscan_tdo             : out std_logic;
    s_bscan_tms             : in std_logic;
    s_bscan_update          : in std_logic;
-- Microblaze Debug Module port
    mdm_SYS_Rst             : in std_logic;
    mdm_Clk                 : in std_logic;
    mdm_TDI                 : in std_logic;
    mdm_TDO                 : out std_logic;
    mdm_Reg_En              : in std_logic_vector(0 to 7);
    mdm_Capture             : in std_logic;
    mdm_Shift               : in std_logic;
    mdm_Update              : in std_logic;
    mdm_Rst                 : in std_logic;
    mdm_Disable             : in std_logic;
-- general purpose registers, accessible through the I/O space
    gpo_reg0            : in std_logic_vector(63 downto 0);
    gpo_reg1            : in std_logic_vector(63 downto 0);
    gpo_reg2            : in std_logic_vector(63 downto 0);
    gpo_reg3            : in std_logic_vector(63 downto 0);
    gpo_reg4            : in std_logic_vector(63 downto 0);
    gpo_reg5            : in std_logic_vector(63 downto 0);
    gpo_reg6            : in std_logic_vector(63 downto 0);
    gpo_reg7            : in std_logic_vector(63 downto 0);
    gpo_reg8            : in std_logic_vector(63 downto 0);
    gpo_reg9            : in std_logic_vector(63 downto 0);
    gpo_reg10           : in std_logic_vector(63 downto 0);
    gpo_reg11           : in std_logic_vector(63 downto 0);
    gpo_reg12           : in std_logic_vector(63 downto 0);
    gpo_reg13           : in std_logic_vector(63 downto 0);
    gpo_reg14           : in std_logic_vector(63 downto 0);
    gpo_reg15           : in std_logic_vector(63 downto 0);
    gpi_reg0            : out std_logic_vector(63 downto 0);
    gpi_reg1            : out std_logic_vector(63 downto 0);
    gpi_reg2            : out std_logic_vector(63 downto 0);
    gpi_reg3            : out std_logic_vector(63 downto 0);
    gpi_reg4            : out std_logic_vector(63 downto 0);
    gpi_reg5            : out std_logic_vector(63 downto 0);
    gpi_reg6            : out std_logic_vector(63 downto 0);
    gpi_reg7            : out std_logic_vector(63 downto 0);
    gpi_reg8            : out std_logic_vector(63 downto 0);
    gpi_reg9            : out std_logic_vector(63 downto 0);
    gpi_reg10           : out std_logic_vector(63 downto 0);
    gpi_reg11           : out std_logic_vector(63 downto 0);
    gpi_reg12           : out std_logic_vector(63 downto 0);
    gpi_reg13           : out std_logic_vector(63 downto 0);
    gpi_reg14           : out std_logic_vector(63 downto 0);
    gpi_reg15           : out std_logic_vector(63 downto 0);
-- DDR4
    F_D1_ACT_N : out std_logic;
    F_D1_A : out std_logic_vector ( 17 downto 0 );
    F_D1_BA : out std_logic_vector ( 1 downto 0 );
    F_D1_BG : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D1_CKE : out std_logic_vector ( 1 downto 0 );
    F_D1_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D1_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D1_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D1_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D1_ODT : out std_logic_vector ( 1 downto 0 );
    F_D1_PARITY_N : out std_logic;
    F_D1_RESET_N : out std_logic;
    F_D1C_CLK_N : in std_logic;
    F_D1C_CLK_P : in std_logic;

    F_D2_ACT_N : out std_logic;
    F_D2_A : out std_logic_vector ( 17 downto 0 );
    F_D2_BA : out std_logic_vector ( 1 downto 0 );
    F_D2_BG : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D2_CKE : out std_logic_vector ( 1 downto 0 );
    F_D2_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D2_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D2_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D2_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D2_ODT : out std_logic_vector ( 1 downto 0 );
    F_D2_PARITY_N : out std_logic;
    F_D2_RESET_N : out std_logic;
    F_D2C_CLK_N : in std_logic;
    F_D2C_CLK_P : in std_logic;

    F_D3_ACT_N : out std_logic;
    F_D3_A : out std_logic_vector ( 17 downto 0 );
    F_D3_BA : out std_logic_vector ( 1 downto 0 );
    F_D3_BG : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D3_CKE : out std_logic_vector ( 1 downto 0 );
    F_D3_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D3_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D3_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D3_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D3_ODT : out std_logic_vector ( 1 downto 0 );
    F_D3_PARITY_N : out std_logic;
    F_D3_RESET_N : out std_logic;
    F_D3C_CLK_N : in std_logic;
    F_D3C_CLK_P : in std_logic;

    F_D4_ACT_N : out std_logic;
    F_D4_A : out std_logic_vector ( 17 downto 0 );
    F_D4_BA : out std_logic_vector ( 1 downto 0 );
    F_D4_BG : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D4_CKE : out std_logic_vector ( 1 downto 0 );
    F_D4_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D4_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D4_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D4_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D4_ODT : out std_logic_vector ( 1 downto 0 );
    F_D4_PARITY_N : out std_logic;
    F_D4_RESET_N : out std_logic;
    F_D4C_CLK_N : in std_logic;
    F_D4C_CLK_P : in std_logic;
-- CMAC
    F_MAC0C_CLK_P   : in std_logic;
    F_MAC0C_CLK_N   : in std_logic;
    F_MAC0_TX_P : out std_logic_vector(3 downto 0);
    F_MAC0_TX_N : out std_logic_vector(3 downto 0);
    F_MAC0_RX_P : in std_logic_vector(3 downto 0);
    F_MAC0_RX_N : in std_logic_vector(3 downto 0);

    F_MAC1C_CLK_P   : in std_logic;
    F_MAC1C_CLK_N   : in std_logic;
    F_MAC1_TX_P : out std_logic_vector(3 downto 0);
    F_MAC1_TX_N : out std_logic_vector(3 downto 0);
    F_MAC1_RX_P : in std_logic_vector(3 downto 0);
    F_MAC1_RX_N : in std_logic_vector(3 downto 0);

    F_MAC2C_CLK_P   : in std_logic;
    F_MAC2C_CLK_N   : in std_logic;
    F_MAC2_TX_P : out std_logic_vector(3 downto 0);
    F_MAC2_TX_N : out std_logic_vector(3 downto 0);
    F_MAC2_RX_P : in std_logic_vector(3 downto 0);
    F_MAC2_RX_N : in std_logic_vector(3 downto 0);

    F_MAC3C_CLK_P   : in std_logic;
    F_MAC3C_CLK_N   : in std_logic;
    F_MAC3_TX_P : out std_logic_vector(3 downto 0);
    F_MAC3_TX_N : out std_logic_vector(3 downto 0);
    F_MAC3_RX_P : in std_logic_vector(3 downto 0);
    F_MAC3_RX_N : in std_logic_vector(3 downto 0);
-- PCIE x16
    F_PCIE16C_CLK_P   : in std_logic;
    F_PCIE16C_CLK_N   : in std_logic;
    F_PCIE16_TX_P : out std_logic_vector(15 downto 0);
    F_PCIE16_TX_N : out std_logic_vector(15 downto 0);
    F_PCIE16_RX_P : in std_logic_vector(15 downto 0);
    F_PCIE16_RX_N : in std_logic_vector(15 downto 0);
-- NVMe
    F_NVMEC_CLK_P   : in std_logic;
    F_NVMEC_CLK_N   : in std_logic;
    F_NVME_TX_P : out std_logic_vector(3 downto 0);
    F_NVME_TX_N : out std_logic_vector(3 downto 0);
    F_NVME_RX_P : in std_logic_vector(3 downto 0);
    F_NVME_RX_N : in std_logic_vector(3 downto 0);
-- C2C
    B_C2CC_CLK_P    : in std_logic;
    B_C2CC_CLK_N    : in std_logic;
    B_C2C_TX_P      : in std_logic_vector(0 downto 0);
    B_C2C_TX_N      : in std_logic_vector(0 downto 0);
    B_C2C_RX_P      : out std_logic_vector(0 downto 0);
    B_C2C_RX_N      : out std_logic_vector(0 downto 0);
    B_C2C_NMI       : in std_logic;
-- I2C
    F_I2C0_SDA      : inout std_logic;
    F_I2C0_SCL      : inout std_logic;

    F_I2C1_SDA      : inout std_logic;
    F_I2C1_SCL      : inout std_logic;

    F_I2C2_SDA      : inout std_logic;
    F_I2C2_SCL      : inout std_logic;

    F_I2C3_SDA      : inout std_logic;
    F_I2C3_SCL      : inout std_logic;

    F_I2C4_SDA      : inout std_logic;
    F_I2C4_SCL      : inout std_logic;

    F_I2C5_SDA      : inout std_logic;
    F_I2C5_SCL      : inout std_logic;
    F_I2C5_RESET_N  : out std_logic;
    F_I2C5_INT_N    : in std_logic;
-- FUART
    B_FUART_TXD     : in std_logic;
    B_FUART_RXD     : out std_logic;
    B_FUART_RTS     : in std_logic;
    B_FUART_CTS     : out std_logic;
-- IRQ
    F_IRQ_IRQ0      : out std_logic;
    F_IRQ_IRQ1      : out std_logic;
    F_IRQ_IRQ2      : out std_logic;
    F_IRQ_IRQ3      : out std_logic
);
end enzian_app_dcs;

architecture Behavioral of enzian_app_dcs is

component eci_gateway is
generic (
    TX_NO_CHANNELS      : integer;
    RX_NO_CHANNELS      : integer;
    RX_FILTER_VC        : VC_BITFIELDS;
    RX_FILTER_TYPE_MASK : ECI_TYPE_MASKS;
    RX_FILTER_TYPE      : ECI_TYPE_MASKS;
    RX_FILTER_CLI_MASK  : CLI_ARRAY;
    RX_FILTER_CLI       : CLI_ARRAY
);
port (
    clk_sys                 : in std_logic;
    clk_io_out              : out std_logic;
    clk_prgc0_out           : out std_logic;
    clk_prgc1_out           : out std_logic;

    prgc0_clk_p             : in std_logic;
    prgc0_clk_n             : in std_logic;
    prgc1_clk_p             : in std_logic;
    prgc1_clk_n             : in std_logic;

    reset_sys               : in std_logic;
    reset_out               : out std_logic;
    reset_n_out             : out std_logic;
    link1_up                : in std_logic;
    link2_up                : in std_logic;

    link1_in_data           : in std_logic_vector(447 downto 0);
    link1_in_vc_no          : in std_logic_vector(27 downto 0);
    link1_in_we2            : in std_logic_vector(6 downto 0);
    link1_in_we3            : in std_logic_vector(6 downto 0);
    link1_in_we4            : in std_logic_vector(6 downto 0);
    link1_in_we5            : in std_logic_vector(6 downto 0);
    link1_in_valid          : in std_logic;
    link1_in_credit_return  : out std_logic_vector(12 downto 2);

    link1_out_hi_data       : out std_logic_vector(575 downto 0);
    link1_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_hi_size       : out std_logic_vector(2 downto 0);
    link1_out_hi_valid      : out std_logic;
    link1_out_hi_ready      : in std_logic;

    link1_out_lo_data       : out std_logic_vector(63 downto 0);
    link1_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_lo_valid      : out std_logic;
    link1_out_lo_ready      : in std_logic;
    link1_out_credit_return : in std_logic_vector(12 downto 2);

    link2_in_data           : in std_logic_vector(447 downto 0);
    link2_in_vc_no          : in std_logic_vector(27 downto 0);
    link2_in_we2            : in std_logic_vector(6 downto 0);
    link2_in_we3            : in std_logic_vector(6 downto 0);
    link2_in_we4            : in std_logic_vector(6 downto 0);
    link2_in_we5            : in std_logic_vector(6 downto 0);
    link2_in_valid          : in std_logic;
    link2_in_credit_return  : out std_logic_vector(12 downto 2);

    link2_out_hi_data       : out std_logic_vector(575 downto 0);
    link2_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_hi_size       : out std_logic_vector(2 downto 0);
    link2_out_hi_valid      : out std_logic;
    link2_out_hi_ready      : in std_logic;

    link2_out_lo_data       : out std_logic_vector(63 downto 0);
    link2_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_lo_valid      : out std_logic;
    link2_out_lo_ready      : in std_logic;
    link2_out_credit_return : in std_logic_vector(12 downto 2);

    s_bscan_bscanid_en      : in std_logic;
    s_bscan_capture         : in std_logic;
    s_bscan_drck            : in std_logic;
    s_bscan_reset           : in std_logic;
    s_bscan_runtest         : in std_logic;
    s_bscan_sel             : in std_logic;
    s_bscan_shift           : in std_logic;
    s_bscan_tck             : in std_logic;
    s_bscan_tdi             : in std_logic;
    s_bscan_tdo             : out std_logic;
    s_bscan_tms             : in std_logic;
    s_bscan_update          : in std_logic;

    m0_bscan_bscanid_en     : out std_logic;
    m0_bscan_capture        : out std_logic;
    m0_bscan_drck           : out std_logic;
    m0_bscan_reset          : out std_logic;
    m0_bscan_runtest        : out std_logic;
    m0_bscan_sel            : out std_logic;
    m0_bscan_shift          : out std_logic;
    m0_bscan_tck            : out std_logic;
    m0_bscan_tdi            : out std_logic;
    m0_bscan_tdo            : in std_logic;
    m0_bscan_tms            : out std_logic;
    m0_bscan_update         : out std_logic;

    rx_eci_channels         : out ARRAY_ECI_CHANNELS(RX_NO_CHANNELS-1 downto 0);
    rx_eci_channels_ready   : in std_logic_vector(RX_NO_CHANNELS-1 downto 0);

    tx_eci_channels         : in ARRAY_ECI_CHANNELS(TX_NO_CHANNELS-1 downto 0);
    tx_eci_channels_ready   : out std_logic_vector(TX_NO_CHANNELS-1 downto 0)
);
end component;

component eci_channel_bus_converter is
port (
    clk             : in STD_LOGIC;

    in_channel      : in ECI_CHANNEL;
    in_ready        : out STD_LOGIC;

    out_data        : out WORDS(16 downto 0);
    out_vc_no       : out std_logic_vector(3 downto 0);
    out_size        : out std_logic_vector(4 downto 0);
    out_valid       : out std_logic;
    out_ready       : in std_logic
);
end component;

component eci_bus_channel_converter is
port (
    clk             : in STD_LOGIC;

    in_data         : in WORDS(16 downto 0);
    in_vc_no        : in std_logic_vector(3 downto 0);
    in_size         : in std_logic_vector(4 downto 0);
    in_valid        : in std_logic;
    in_ready        : out std_logic;

    out_channel     : out ECI_CHANNEL;
    out_ready       : in STD_LOGIC
);
end component;

component loopback_vc_resp_nodata is
generic (
   WORD_WIDTH : integer;
   GSDN_GSYNC_FN : integer
);
port (
    clk, reset : in std_logic;

    -- ECI Request input stream
    vc_req_i       : in  std_logic_vector(63 downto 0);
    vc_req_valid_i : in  std_logic;
    vc_req_ready_o : out std_logic;

    -- ECI Response output stream
    vc_resp_o       : out std_logic_vector(63 downto 0);
    vc_resp_valid_o : out std_logic;
    vc_resp_ready_i : in  std_logic
);
end component;

type dc_tracing_cli_array is array (integer range <>) of std_logic_vector(39 downto 0);
type dc_tracing_state_array is array (integer range <>) of std_logic_vector(6 downto 0);
type dc_tracing_action_array is array (integer range <>) of std_logic_vector(3 downto 0);
type dc_tracing_request_array is array (integer range <>) of std_logic_vector(4 downto 0);

component dcs_2_axi is
port (
  clk, reset : in std_logic;

  -- Input ECI events.
  -- ECI packet for request without data. (VC 6 or 7) (only header).
  req_wod_hdr_i       : in std_logic_vector(63 downto 0);
  req_wod_pkt_size_i  : in std_logic_vector( 4 downto 0);
  req_wod_pkt_vc_i    : in std_logic_vector( 3 downto 0);
  req_wod_pkt_valid_i : in std_logic;
  req_wod_pkt_ready_o : out std_logic;

  -- ECI packet for response without data.(VC 10 or 11). (only header).
  rsp_wod_hdr_i       : in std_logic_vector(63 downto 0);
  rsp_wod_pkt_size_i  : in std_logic_vector( 4 downto 0);
  rsp_wod_pkt_vc_i    : in std_logic_vector( 3 downto 0);
  rsp_wod_pkt_valid_i : in std_logic;
  rsp_wod_pkt_ready_o : out std_logic;

  -- ECI packet for response with data. (VC 4 or 5). (header + data).
  rsp_wd_pkt_i        : in std_logic_vector(17*64-1 downto 0);
  rsp_wd_pkt_size_i   : in std_logic_vector( 4 downto 0);
  rsp_wd_pkt_vc_i     : in std_logic_vector( 3 downto 0);
  rsp_wd_pkt_valid_i  : in std_logic;
  rsp_wd_pkt_ready_o  : out std_logic;

  -- ECI packet for local forward without data. (VC 16 or 17).
  -- lcl clean, lcl clean inv requests.
  lcl_fwd_wod_hdr_i       : in std_logic_vector(63 downto 0);
  lcl_fwd_wod_pkt_size_i  : in std_logic_vector( 4 downto 0);
  lcl_fwd_wod_pkt_vc_i    : in std_logic_vector( 4 downto 0); --5 bits not 4.
  lcl_fwd_wod_pkt_valid_i : in std_logic;
  lcl_fwd_wod_pkt_ready_o : out std_logic;

  -- ECI packet for local rsp without data. (VC 18 or 19).
  -- lcl unlock response message. 
  lcl_rsp_wod_hdr_i       : in std_logic_vector(63 downto 0);
  lcl_rsp_wod_pkt_size_i  : in std_logic_vector( 4 downto 0);
  lcl_rsp_wod_pkt_vc_i    : in std_logic_vector( 4 downto 0); --5 bits not 4.
  lcl_rsp_wod_pkt_valid_i : in std_logic;
  lcl_rsp_wod_pkt_ready_o : out std_logic;

  -- Output ECI events. (rsp without data, rsp with data).
  -- VC 10,11
  rsp_wod_hdr_o       : out std_logic_vector(63 downto 0);
  rsp_wod_pkt_size_o  : out std_logic_vector( 4 downto 0);
  rsp_wod_pkt_vc_o    : out std_logic_vector( 3 downto 0);
  rsp_wod_pkt_valid_o : out std_logic;
  rsp_wod_pkt_ready_i : in std_logic;

  -- Responses with data (VC 5 or 4)
  -- header+payload
  rsp_wd_pkt_o       : out std_logic_vector(17*64-1 downto 0);
  rsp_wd_pkt_size_o  : out std_logic_vector( 4 downto 0);
  rsp_wd_pkt_vc_o    : out std_logic_vector( 3 downto 0);
  rsp_wd_pkt_valid_o : out std_logic;
  rsp_wd_pkt_ready_i : in std_logic;

  -- forwards without data (VC 8 or 9).
  fwd_wod_hdr_o       : out std_logic_vector(63 downto 0);
  fwd_wod_pkt_size_o  : out std_logic_vector( 4 downto 0);
  fwd_wod_pkt_vc_o    : out std_logic_vector( 3 downto 0);
  fwd_wod_pkt_valid_o : out std_logic;
  fwd_wod_pkt_ready_i : in std_logic;

  -- lcl responses without data (VC 18 or 19)
  lcl_rsp_wod_hdr_o       : out std_logic_vector(63 downto 0);
  lcl_rsp_wod_pkt_size_o  : out std_logic_vector( 4 downto 0);
  lcl_rsp_wod_pkt_vc_o    : out std_logic_vector( 4 downto 0); --5 bits not 4.
  lcl_rsp_wod_pkt_valid_o : out std_logic;
  lcl_rsp_wod_pkt_ready_i : in std_logic;

  -- Primary AXI rd/wr i/f.
  p_axi_arid    : out std_logic_vector( 6 downto 0);
  p_axi_araddr  : out std_logic_vector(37 downto 0);
  p_axi_arlen   : out std_logic_vector( 7 downto 0);
  p_axi_arsize  : out std_logic_vector( 2 downto 0);
  p_axi_arburst : out std_logic_vector( 1 downto 0);
  p_axi_arlock  : out std_logic;
  p_axi_arcache : out std_logic_vector( 3 downto 0);
  p_axi_arprot  : out std_logic_vector( 2 downto 0);
  p_axi_arvalid : out std_logic;
  p_axi_arready : in std_logic;
  p_axi_rid     : in std_logic_vector( 6 downto 0);
  p_axi_rdata   : in std_logic_vector(511 downto 0);
  p_axi_rresp   : in std_logic_vector( 1 downto 0);
  p_axi_rlast   : in std_logic;
  p_axi_rvalid  : in std_logic;
  p_axi_rready  : out std_logic;

  p_axi_awid    : out std_logic_vector ( 6 downto 0);
  p_axi_awaddr  : out std_logic_vector (37 downto 0);
  p_axi_awlen   : out std_logic_vector ( 7 downto 0);
  p_axi_awsize  : out std_logic_vector ( 2 downto 0);
  p_axi_awburst : out std_logic_vector ( 1 downto 0);
  p_axi_awlock  : out std_logic;
  p_axi_awcache : out std_logic_vector ( 3 downto 0);
  p_axi_awprot  : out std_logic_vector ( 2 downto 0);
  p_axi_awvalid : out std_logic;
  p_axi_awready : in std_logic ;
  p_axi_wdata   : out std_logic_vector (511 downto 0);
  p_axi_wstrb   : out std_logic_vector (63 downto 0);
  p_axi_wlast   : out std_logic;
  p_axi_wvalid  : out std_logic;
  p_axi_wready  : in std_logic;
  p_axi_bid     : in std_logic_vector( 6 downto 0);
  p_axi_bresp   : in std_logic_vector( 1 downto 0);
  p_axi_bvalid  : in std_logic;
  p_axi_bready  : out std_logic;
-- Tracing
    tracing_valid   : out std_logic_vector(1 downto 0);
    tracing_error   : out std_logic_vector(1 downto 0);
    tracing_cli     : out dc_tracing_cli_array(1 downto 0);
    tracing_state   : out dc_tracing_state_array(1 downto 0);
    tracing_action  : out dc_tracing_action_array(1 downto 0);
    tracing_request : out dc_tracing_request_array(1 downto 0)
);
end component;

-- DDR block design.
component  design_1 is
  port (
    clk_sys : in STD_LOGIC;

    ddr4_c1_axi_araddr : in STD_LOGIC_VECTOR ( 37 downto 0 );
    ddr4_c1_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_arid : in STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c1_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ddr4_c1_axi_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c1_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_arready : out STD_LOGIC;
    ddr4_c1_axi_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c1_axi_arvalid : in STD_LOGIC;
    ddr4_c1_axi_awaddr : in STD_LOGIC_VECTOR ( 37 downto 0 );
    ddr4_c1_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_awid : in STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c1_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ddr4_c1_axi_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c1_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_awready : out STD_LOGIC;
    ddr4_c1_axi_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c1_axi_awvalid : in STD_LOGIC;
    ddr4_c1_axi_bid : out STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c1_axi_bready : in STD_LOGIC;
    ddr4_c1_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_bvalid : out STD_LOGIC;
    ddr4_c1_axi_ctrl_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c1_axi_ctrl_arready : out STD_LOGIC;
    ddr4_c1_axi_ctrl_arvalid : in STD_LOGIC;
    ddr4_c1_axi_ctrl_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c1_axi_ctrl_awready : out STD_LOGIC;
    ddr4_c1_axi_ctrl_awvalid : in STD_LOGIC;
    ddr4_c1_axi_ctrl_bready : in STD_LOGIC;
    ddr4_c1_axi_ctrl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_ctrl_bvalid : out STD_LOGIC;
    ddr4_c1_axi_ctrl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c1_axi_ctrl_rready : in STD_LOGIC;
    ddr4_c1_axi_ctrl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_ctrl_rvalid : out STD_LOGIC;
    ddr4_c1_axi_ctrl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c1_axi_ctrl_wready : out STD_LOGIC;
    ddr4_c1_axi_ctrl_wvalid : in STD_LOGIC;
    ddr4_c1_axi_rdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    ddr4_c1_axi_rid : out STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c1_axi_rlast : out STD_LOGIC;
    ddr4_c1_axi_rready : in STD_LOGIC;
    ddr4_c1_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_rvalid : out STD_LOGIC;
    ddr4_c1_axi_wdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    ddr4_c1_axi_wlast : in STD_LOGIC;
    ddr4_c1_axi_wready : out STD_LOGIC;
    ddr4_c1_axi_wstrb : in STD_LOGIC_VECTOR ( 63 downto 0 );
    ddr4_c1_axi_wvalid : in STD_LOGIC;

    ddr4_c1_act_n : out STD_LOGIC;
    ddr4_c1_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
    ddr4_c1_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_bg : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_ck_c : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_ck_t : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_cke : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_cs_n : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_dq : inout STD_LOGIC_VECTOR ( 71 downto 0 );
    ddr4_c1_dqs_c : inout STD_LOGIC_VECTOR ( 17 downto 0 );
    ddr4_c1_dqs_t : inout STD_LOGIC_VECTOR ( 17 downto 0 );
    ddr4_c1_odt : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_par : out STD_LOGIC;
    ddr4_c1_reset_n : out STD_LOGIC;
    ddr4_c1_sys_clk_n : in STD_LOGIC;
    ddr4_c1_sys_clk_p : in STD_LOGIC;
    ddr4_c1_ui_clk : out STD_LOGIC;

    ddr4_c4_axi_araddr : in STD_LOGIC_VECTOR ( 37 downto 0 );
    ddr4_c4_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_arid : in STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c4_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ddr4_c4_axi_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c4_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_arready : out STD_LOGIC;
    ddr4_c4_axi_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c4_axi_arvalid : in STD_LOGIC;
    ddr4_c4_axi_awaddr : in STD_LOGIC_VECTOR ( 37 downto 0 );
    ddr4_c4_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_awid : in STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c4_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ddr4_c4_axi_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c4_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_awready : out STD_LOGIC;
    ddr4_c4_axi_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c4_axi_awvalid : in STD_LOGIC;
    ddr4_c4_axi_bid : out STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c4_axi_bready : in STD_LOGIC;
    ddr4_c4_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_bvalid : out STD_LOGIC;
    ddr4_c4_axi_ctrl_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c4_axi_ctrl_arready : out STD_LOGIC;
    ddr4_c4_axi_ctrl_arvalid : in STD_LOGIC;
    ddr4_c4_axi_ctrl_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c4_axi_ctrl_awready : out STD_LOGIC;
    ddr4_c4_axi_ctrl_awvalid : in STD_LOGIC;
    ddr4_c4_axi_ctrl_bready : in STD_LOGIC;
    ddr4_c4_axi_ctrl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_ctrl_bvalid : out STD_LOGIC;
    ddr4_c4_axi_ctrl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c4_axi_ctrl_rready : in STD_LOGIC;
    ddr4_c4_axi_ctrl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_ctrl_rvalid : out STD_LOGIC;
    ddr4_c4_axi_ctrl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c4_axi_ctrl_wready : out STD_LOGIC;
    ddr4_c4_axi_ctrl_wvalid : in STD_LOGIC;
    ddr4_c4_axi_rdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    ddr4_c4_axi_rid : out STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c4_axi_rlast : out STD_LOGIC;
    ddr4_c4_axi_rready : in STD_LOGIC;
    ddr4_c4_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_rvalid : out STD_LOGIC;
    ddr4_c4_axi_wdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    ddr4_c4_axi_wlast : in STD_LOGIC;
    ddr4_c4_axi_wready : out STD_LOGIC;
    ddr4_c4_axi_wstrb : in STD_LOGIC_VECTOR ( 63 downto 0 );
    ddr4_c4_axi_wvalid : in STD_LOGIC;

    ddr4_c4_act_n : out STD_LOGIC;
    ddr4_c4_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
    ddr4_c4_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_bg : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_ck_c : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_ck_t : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_cke : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_cs_n : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_dq : inout STD_LOGIC_VECTOR ( 71 downto 0 );
    ddr4_c4_dqs_c : inout STD_LOGIC_VECTOR ( 17 downto 0 );
    ddr4_c4_dqs_t : inout STD_LOGIC_VECTOR ( 17 downto 0 );
    ddr4_c4_odt : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_par : out STD_LOGIC;
    ddr4_c4_reset_n : out STD_LOGIC;
    ddr4_c4_sys_clk_n : in STD_LOGIC;
    ddr4_c4_sys_clk_p : in STD_LOGIC;
    ddr4_c4_ui_clk : out STD_LOGIC;
    ddr4_sys_rst : in STD_LOGIC;
    reset_n : in STD_LOGIC
  );
end component;

-- BRAM to connect to DC slices.
component axi_bram_ctrl_0 is
port (
    s_axi_aclk    : IN STD_LOGIC;
    s_axi_aresetn : IN STD_LOGIC;
    s_axi_awid    : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    s_axi_awaddr  : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axi_awlen   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axi_awsize  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_awlock  : IN STD_LOGIC;
    s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_awprot  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_awvalid : IN STD_LOGIC;
    s_axi_awready : OUT STD_LOGIC;
    s_axi_wdata   : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axi_wstrb   : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axi_wlast   : IN STD_LOGIC;
    s_axi_wvalid  : IN STD_LOGIC;
    s_axi_wready  : OUT STD_LOGIC;
    s_axi_bid     : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    s_axi_bresp   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_bvalid  : OUT STD_LOGIC;
    s_axi_bready  : IN STD_LOGIC;
    s_axi_arid    : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    s_axi_araddr  : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axi_arlen   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axi_arsize  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_arlock  : IN STD_LOGIC;
    s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_arprot  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_arvalid : IN STD_LOGIC;
    s_axi_arready : OUT STD_LOGIC;
    s_axi_rid     : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    s_axi_rdata   : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axi_rresp   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_rlast   : OUT STD_LOGIC;
    s_axi_rvalid  : OUT STD_LOGIC;
    s_axi_rready  : IN STD_LOGIC
  );
end component;

-- component for LCI perf measurements.
-- ILAs are added inside.
component lci_perf is
  generic (
    LCI_LAT_TPUT      : integer;
    NUM_REQS_TO_WAIT  : integer;
    NUM_ADDR_TO_ISSUE : integer;
    PERF_REGS_WIDTH   : integer;
    FIFO_DEPTH        : integer
    );
  port (
    clk, reset            : in std_logic;
    odd_wait_req_valid_i  : in std_logic;
    even_wait_req_valid_i : in std_logic;
    -- outgoing lci for odd cls or even VC 16.
    lci_odd_idx_hdr_o     : out std_logic_vector(63 downto 0);
    lci_odd_idx_size_o    : out std_logic_vector(4 downto 0);
    lci_odd_idx_vc_o      : out std_logic_vector(4 downto 0);
    lci_odd_idx_valid_o   : out std_logic;
    lci_odd_idx_ready_i   : in std_logic;
    -- incoming lcia for odd cls from even VC 18.
    lcia_odd_idx_hdr_i    : in std_logic_vector(63 downto 0);
    lcia_odd_idx_size_i   : in std_logic_vector(4 downto 0);
    lcia_odd_idx_vc_i     : in std_logic_vector(4 downto 0);
    lcia_odd_idx_valid_i  : in std_logic;
    lcia_odd_idx_ready_o  : out std_logic;
    -- outgoing unlock for odd cls to even VC 18.
    ul_odd_idx_hdr_o      : out std_logic_vector(63 downto 0);
    ul_odd_idx_size_o     : out std_logic_vector(4 downto 0);
    ul_odd_idx_vc_o       : out std_logic_vector(4 downto 0);
    ul_odd_idx_valid_o    : out std_logic;
    ul_odd_idx_ready_i    : in std_logic;
    -- outgoing lci for even cls or even VC 17.
    lci_even_idx_hdr_o    : out std_logic_vector(63 downto 0);
    lci_even_idx_size_o   : out std_logic_vector(4 downto 0);
    lci_even_idx_vc_o     : out std_logic_vector(4 downto 0);
    lci_even_idx_valid_o  : out std_logic;
    lci_even_idx_ready_i  : in std_logic;
    -- incoming lcia for even cls from even VC 19.
    lcia_even_idx_hdr_i   : in std_logic_vector(63 downto 0);
    lcia_even_idx_size_i  : in std_logic_vector(4 downto 0);
    lcia_even_idx_vc_i    : in std_logic_vector(4 downto 0);
    lcia_even_idx_valid_i : in std_logic;
    lcia_even_idx_ready_o : out std_logic;
    -- outgoing unlock for even cls to even VC 19.
    ul_even_idx_hdr_o     : out std_logic_vector(63 downto 0);
    ul_even_idx_size_o    : out std_logic_vector(4 downto 0);
    ul_even_idx_vc_o      : out std_logic_vector(4 downto 0);
    ul_even_idx_valid_o   : out std_logic;
    ul_even_idx_ready_i   : in std_logic;
    -- completion indicator.
    done_o                : out std_logic
    );
end component;

type ECI_PACKET_RX is record
    c6_gsync            : ECI_CHANNEL;
    c6_gsync_ready      : std_logic;
    c7_gsync            : ECI_CHANNEL;
    c7_gsync_ready      : std_logic;
    ginv                : ECI_CHANNEL;
    -- RX req_wod VC 6,7
    dcs_c6              : ECI_CHANNEL;
    dcs_c6_ready        : std_logic;
    dcs_c7              : ECI_CHANNEL;
    dcs_c7_ready        : std_logic;
    -- RX rsp_wod VC 10,11
    dcs_c10             : ECI_CHANNEL;
    dcs_c10_ready       : std_logic;
    dcs_c11             : ECI_CHANNEL;
    dcs_c11_ready       : std_logic;
    -- RX rsp_wd VC 4,5
    dcs_c4              : ECI_CHANNEL;
    dcs_c4_ready        : std_logic;
    dcs_c5              : ECI_CHANNEL;
    dcs_c5_ready        : std_logic;
    -- RX rsp_wd VC 4,5 ECI packet.
    dcs_c4_wd_pkt       : WORDS(16 downto 0);
    dcs_c4_wd_pkt_size  : std_logic_vector(4 downto 0);
    dcs_c4_wd_pkt_vc    : std_logic_vector(3 downto 0);
    dcs_c4_wd_pkt_valid : std_logic;
    dcs_c4_wd_pkt_ready : std_logic;
    dcs_c5_wd_pkt       : WORDS(16 downto 0);
    dcs_c5_wd_pkt_size  : std_logic_vector(4 downto 0);
    dcs_c5_wd_pkt_vc    : std_logic_vector(3 downto 0);
    dcs_c5_wd_pkt_valid : std_logic;
    dcs_c5_wd_pkt_ready : std_logic;
end record ECI_PACKET_RX;

type ECI_PACKET_TX is record
    c10_gsync           : ECI_CHANNEL;
    c10_gsync_ready     : std_logic;
    c11_gsync           : ECI_CHANNEL;
    c11_gsync_ready     : std_logic;
    -- TX rsp_wod VC 10,11
    dcs_c10             : ECI_CHANNEL;
    dcs_c10_ready       : std_logic;
    dcs_c11             : ECI_CHANNEL;
    dcs_c11_ready       : std_logic;
    -- TX rsp_wd VC 4,5
    dcs_c4              : ECI_CHANNEL;
    dcs_c4_ready        : std_logic;
    dcs_c5              : ECI_CHANNEL;
    dcs_c5_ready        : std_logic;
    -- TX rsp_wd VC 4,5 ECI packet.
    dcs_c4_wd_pkt       : std_logic_vector(17*64-1 downto 0);
    dcs_c4_wd_pkt_size  : std_logic_vector(4 downto 0);
    dcs_c4_wd_pkt_vc    : std_logic_vector(3 downto 0);
    dcs_c4_wd_pkt_valid : std_logic;
    dcs_c4_wd_pkt_ready : std_logic;
    dcs_c5_wd_pkt       : std_logic_vector(17*64-1 downto 0);
    dcs_c5_wd_pkt_size  : std_logic_vector(4 downto 0);
    dcs_c5_wd_pkt_vc    : std_logic_vector(3 downto 0);
    dcs_c5_wd_pkt_valid : std_logic;
    dcs_c5_wd_pkt_ready : std_logic;
    -- TX fwd_wod VC 8.9
    dcs_c8              : ECI_CHANNEL;
    dcs_c8_ready        : std_logic;
    dcs_c9              : ECI_CHANNEL;
    dcs_c9_ready        : std_logic;
    mux1                : ECI_CHANNEL;
    mux1_ready          : std_logic;
    mux2                : ECI_CHANNEL;
    mux2_ready          : std_logic;
end record ECI_PACKET_TX;

type DCS_AXI is record
    arid    : std_logic_vector( 6 downto 0);
    araddr  : std_logic_vector(37 downto 0);
    arlen   : std_logic_vector( 7 downto 0);
    arsize  : std_logic_vector( 2 downto 0);
    arburst : std_logic_vector( 1 downto 0);
    arlock  : std_logic;
    arcache : std_logic_vector( 3 downto 0);
    arprot  : std_logic_vector( 2 downto 0);
    arvalid : std_logic;
    arready : std_logic;
    rid     : std_logic_vector( 6 downto 0);
    rdata   : std_logic_vector(511 downto 0);
    rresp   : std_logic_vector( 1 downto 0);
    rlast   : std_logic;
    rvalid  : std_logic;
    rready  : std_logic;

    awid    : std_logic_vector ( 6 downto 0);
    awaddr  : std_logic_vector (37 downto 0);
    awlen   : std_logic_vector ( 7 downto 0);
    awsize  : std_logic_vector ( 2 downto 0);
    awburst : std_logic_vector ( 1 downto 0);
    awlock  : std_logic;
    awcache : std_logic_vector ( 3 downto 0);
    awprot  : std_logic_vector ( 2 downto 0);
    awvalid : std_logic;
    awready : std_logic ;
    wdata   : std_logic_vector (511 downto 0);
    wstrb   : std_logic_vector (63 downto 0);
    wlast   : std_logic;
    wvalid  : std_logic;
    wready  : std_logic;
    bid     : std_logic_vector( 6 downto 0);
    bresp   : std_logic_vector( 1 downto 0);
    bvalid  : std_logic;
    bready  : std_logic;
end record DCS_AXI;

type BSCAN is record
    bscanid_en     : std_logic;
    capture        : std_logic;
    drck           : std_logic;
    reset          : std_logic;
    runtest        : std_logic;
    sel            : std_logic;
    shift          : std_logic;
    tck            : std_logic;
    tdi            : std_logic;
    tdo            : std_logic;
    tms            : std_logic;
    update         : std_logic;
end record BSCAN;

type LCL_CHANNEL is record
  data  : std_logic_vector(63 downto 0);
  size  : std_logic_vector(4 downto 0);
  vc_no : std_logic_vector(4 downto 0);
  valid : std_logic;
  ready : std_logic;
end record LCL_CHANNEL;

signal m0_bscan : BSCAN;

signal link_eci_packet_rx : ECI_PACKET_RX;
signal link_eci_packet_tx : ECI_PACKET_TX;

signal clk, clk_io : std_logic;
signal reset : std_logic;
signal reset_n : std_logic;

signal dcs_even_axi, dcs_odd_axi : DCS_AXI;

signal a, b, c, d : std_logic;

-- perf signals.
signal dcs_c16_i               : LCL_CHANNEL; -- LCL FWD WOD
signal dcs_c17_i               : LCL_CHANNEL; -- LCL FWD WOD
signal dcs_c18_o               : LCL_CHANNEL; -- LCL RSP WOD
signal dcs_c19_o               : LCL_CHANNEL; -- LCL RSP WOD
signal dcs_c18_i               : LCL_CHANNEL; -- LCL RSP WOD
signal dcs_c19_i               : LCL_CHANNEL; -- LCL RSP WOD
signal dcs_even_ar_hs: std_logic;
signal dcs_odd_ar_hs: std_logic;

-- tracing signals
signal dc_tracing_valid   : std_logic_vector(3 downto 0);
signal dc_tracing_error   : std_logic_vector(3 downto 0);
signal dc_tracing_cli     : dc_tracing_cli_array(3 downto 0);
signal dc_tracing_state   : dc_tracing_state_array(3 downto 0);
signal dc_tracing_action  : dc_tracing_action_array(3 downto 0);
signal dc_tracing_request : dc_tracing_request_array(3 downto 0);

-- Shift the address right by 1, ignoring lower 7 bits
-- Since there are 2 DC units, one for odd cache line indices and another for even cache line indices,
-- each DC unit generates AXI address with stride 256 bytes, although which half of the 256 block is used changes, so only half of the address space is used
-- To use the whole available address space, the address is divided by 2

function shift_address(ia : std_logic_vector(37 downto 0))
    return std_logic_vector is
    variable a : std_logic_vector(37 downto 0) := (others => '0');
begin
    a(36 downto 7) := ia(37 downto 8);
    return a;
end shift_address;

begin

clk <= clk_sys;

i_eci_gateway : eci_gateway
  generic map (
    TX_NO_CHANNELS      => 4,
    RX_NO_CHANNELS      => 9,
    RX_FILTER_VC        => ("00000100000", "00000010000", "00000110000", "00000100000", "00000010000", "01000000000", "00100000000", "00000001000", "00000000100"),
    RX_FILTER_TYPE_MASK => ("11111", "11111", "11111", "00000", "00000", "00000", "00000", "00000", "00000"),
    RX_FILTER_TYPE      => ("11000", "11000", "10100", "00000", "00000", "00000", "00000", "00000", "00000"),
    RX_FILTER_CLI_MASK  => ((others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0')),
    RX_FILTER_CLI       => ((others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'))
)
port map (
    clk_sys                 => clk,
    clk_io_out              => clk_io,

    prgc0_clk_p             => prgc0_clk_p,
    prgc0_clk_n             => prgc0_clk_n,
    prgc1_clk_p             => prgc1_clk_p,
    prgc1_clk_n             => prgc1_clk_n,

    reset_sys               => reset_sys,
    reset_out               => reset,
    reset_n_out             => reset_n,
    link1_up                => link1_up,
    link2_up                => link2_up,

    link1_in_data           => link1_in_data,
    link1_in_vc_no          => link1_in_vc_no,
    link1_in_we2            => link1_in_we2,
    link1_in_we3            => link1_in_we3,
    link1_in_we4            => link1_in_we4,
    link1_in_we5            => link1_in_we5,
    link1_in_valid          => link1_in_valid,
    link1_in_credit_return  => link1_in_credit_return,

    link1_out_hi_data       => link1_out_hi_data,
    link1_out_hi_vc_no      => link1_out_hi_vc_no,
    link1_out_hi_size       => link1_out_hi_size,
    link1_out_hi_valid      => link1_out_hi_valid,
    link1_out_hi_ready      => link1_out_hi_ready,

    link1_out_lo_data       => link1_out_lo_data,
    link1_out_lo_vc_no      => link1_out_lo_vc_no,
    link1_out_lo_valid      => link1_out_lo_valid,
    link1_out_lo_ready      => link1_out_lo_ready,
    link1_out_credit_return => link1_out_credit_return,

    link2_in_data           => link2_in_data,
    link2_in_vc_no          => link2_in_vc_no,
    link2_in_we2            => link2_in_we2,
    link2_in_we3            => link2_in_we3,
    link2_in_we4            => link2_in_we4,
    link2_in_we5            => link2_in_we5,
    link2_in_valid          => link2_in_valid,
    link2_in_credit_return  => link2_in_credit_return,

    link2_out_hi_data       => link2_out_hi_data,
    link2_out_hi_vc_no      => link2_out_hi_vc_no,
    link2_out_hi_size       => link2_out_hi_size,
    link2_out_hi_valid      => link2_out_hi_valid,
    link2_out_hi_ready      => link2_out_hi_ready,

    link2_out_lo_data       => link2_out_lo_data,
    link2_out_lo_vc_no      => link2_out_lo_vc_no,
    link2_out_lo_valid      => link2_out_lo_valid,
    link2_out_lo_ready      => link2_out_lo_ready,
    link2_out_credit_return => link2_out_credit_return,

    s_bscan_bscanid_en      => s_bscan_bscanid_en,
    s_bscan_capture         => s_bscan_capture,
    s_bscan_drck            => s_bscan_drck,
    s_bscan_reset           => s_bscan_reset,
    s_bscan_runtest         => s_bscan_runtest,
    s_bscan_sel             => s_bscan_sel,
    s_bscan_shift           => s_bscan_shift,
    s_bscan_tck             => s_bscan_tck,
    s_bscan_tdi             => s_bscan_tdi,
    s_bscan_tdo             => s_bscan_tdo,
    s_bscan_tms             => s_bscan_tms,
    s_bscan_update          => s_bscan_update,

    m0_bscan_bscanid_en     => m0_bscan.bscanid_en,
    m0_bscan_capture        => m0_bscan.capture,
    m0_bscan_drck           => m0_bscan.drck,
    m0_bscan_reset          => m0_bscan.reset,
    m0_bscan_runtest        => m0_bscan.runtest,
    m0_bscan_sel            => m0_bscan.sel,
    m0_bscan_shift          => m0_bscan.shift,
    m0_bscan_tck            => m0_bscan.tck,
    m0_bscan_tdi            => m0_bscan.tdi,
    m0_bscan_tdo            => m0_bscan.tdo,
    m0_bscan_tms            => m0_bscan.tms,
    m0_bscan_update         => m0_bscan.update,

    rx_eci_channels(0)      => link_eci_packet_rx.c7_gsync,
    rx_eci_channels(1)      => link_eci_packet_rx.c6_gsync,
    rx_eci_channels(2)      => link_eci_packet_rx.ginv,
    rx_eci_channels(3)      => link_eci_packet_rx.dcs_c7,
    rx_eci_channels(4)      => link_eci_packet_rx.dcs_c6,
    rx_eci_channels(5)      => link_eci_packet_rx.dcs_c11,
    rx_eci_channels(6)      => link_eci_packet_rx.dcs_c10,
    rx_eci_channels(7)      => link_eci_packet_rx.dcs_c5,
    rx_eci_channels(8)      => link_eci_packet_rx.dcs_c4,

    rx_eci_channels_ready(0)   => link_eci_packet_rx.c7_gsync_ready,
    rx_eci_channels_ready(1)   => link_eci_packet_rx.c6_gsync_ready,
    rx_eci_channels_ready(2)   => '1',
    rx_eci_channels_ready(3)   => link_eci_packet_rx.dcs_c7_ready,
    rx_eci_channels_ready(4)   => link_eci_packet_rx.dcs_c6_ready,
    rx_eci_channels_ready(5)   => link_eci_packet_rx.dcs_c11_ready,
    rx_eci_channels_ready(6)   => link_eci_packet_rx.dcs_c10_ready,
    rx_eci_channels_ready(7)   => link_eci_packet_rx.dcs_c5_ready,
    rx_eci_channels_ready(8)   => link_eci_packet_rx.dcs_c4_ready,

    tx_eci_channels(0)      => link_eci_packet_tx.mux1,
    tx_eci_channels(1)      => link_eci_packet_tx.mux2,
    tx_eci_channels(2)      => link_eci_packet_tx.dcs_c5,
    tx_eci_channels(3)      => link_eci_packet_tx.dcs_c4,

    tx_eci_channels_ready(0)   => link_eci_packet_tx.mux1_ready,
    tx_eci_channels_ready(1)   => link_eci_packet_tx.mux2_ready,
    tx_eci_channels_ready(2)   => link_eci_packet_tx.dcs_c5_ready,
    tx_eci_channels_ready(3)   => link_eci_packet_tx.dcs_c4_ready
);

i_tx_mux1 : entity work.eci_channel_muxer
generic map (
    CHANNELS => 3
)
port map (
    clk => clk,
    inputs(0) => link_eci_packet_tx.c11_gsync,
    inputs(1) => link_eci_packet_tx.dcs_c11,
    inputs(2) => link_eci_packet_tx.dcs_c9,
    inputs_ready(0) => link_eci_packet_tx.c11_gsync_ready,
    inputs_ready(1) => link_eci_packet_tx.dcs_c11_ready,
    inputs_ready(2) => link_eci_packet_tx.dcs_c9_ready,
    output => link_eci_packet_tx.mux1,
    output_ready => link_eci_packet_tx.mux1_ready
);
    
i_tx_mux2 : entity work.eci_channel_muxer
generic map (
    CHANNELS => 3
)
port map (
    clk => clk,
    inputs(0) => link_eci_packet_tx.c10_gsync,
    inputs(1) => link_eci_packet_tx.dcs_c10,
    inputs(2) => link_eci_packet_tx.dcs_c8,
    inputs_ready(0) => link_eci_packet_tx.c10_gsync_ready,
    inputs_ready(1) => link_eci_packet_tx.dcs_c10_ready,
    inputs_ready(2) => link_eci_packet_tx.dcs_c8_ready,
    output => link_eci_packet_tx.mux2,
    output_ready => link_eci_packet_tx.mux2_ready
);
    
-- GSYNC response handler, sends GSDN.
-- Odd VCs, GSYNC arrives in VC7 and GSDN sent in VC11.
vc7_vc11_gsync_loopback : loopback_vc_resp_nodata
generic map (
   WORD_WIDTH => 64,
   GSDN_GSYNC_FN => 1
)
port map (
    clk   => clk,
    reset => reset,

    vc_req_i       => link_eci_packet_rx.c7_gsync.data(0),
    vc_req_valid_i => link_eci_packet_rx.c7_gsync.valid,
    vc_req_ready_o => link_eci_packet_rx.c7_gsync_ready,

    vc_resp_o       => link_eci_packet_tx.c11_gsync.data(0),
    vc_resp_valid_o => link_eci_packet_tx.c11_gsync.valid,
    vc_resp_ready_i => link_eci_packet_tx.c11_gsync_ready
);

link_eci_packet_tx.c11_gsync.vc_no <= "1011";
link_eci_packet_tx.c11_gsync.size <= "000";

-- GSYNC response handler, sends GSDN.
-- Even VCs, GSYNC arrives in VC6 and GSDN sent in VC10.
vc6_vc10_gsync_loopback : loopback_vc_resp_nodata
generic map (
   WORD_WIDTH => 64,
   GSDN_GSYNC_FN => 1
)
port map (
    clk   => clk,
    reset => reset,

    vc_req_i       => link_eci_packet_rx.c6_gsync.data(0),
    vc_req_valid_i => link_eci_packet_rx.c6_gsync.valid,
    vc_req_ready_o => link_eci_packet_rx.c6_gsync_ready,

    vc_resp_o       => link_eci_packet_tx.c10_gsync.data(0),
    vc_resp_valid_o => link_eci_packet_tx.c10_gsync.valid,
    vc_resp_ready_i => link_eci_packet_tx.c10_gsync_ready
);

link_eci_packet_tx.c10_gsync.vc_no <= "1010";
link_eci_packet_tx.c10_gsync.size <= "000";

-- RX packetizer.
-- Packetize data from eci_gateway into ECI packet. 
-- RX rsp_wd VC5.
i_dcs_c5_eci_channel_to_bus : eci_channel_bus_converter
port map (
    clk             => clk,
    -- Input eci_gateway packet.
    in_channel      => link_eci_packet_rx.dcs_c5,
    in_ready        => link_eci_packet_rx.dcs_c5_ready,
    -- Output ECI packet.
    out_data        => link_eci_packet_rx.dcs_c5_wd_pkt,
    out_vc_no       => link_eci_packet_rx.dcs_c5_wd_pkt_vc,
    out_size        => link_eci_packet_rx.dcs_c5_wd_pkt_size,
    out_valid       => link_eci_packet_rx.dcs_c5_wd_pkt_valid,
    out_ready       => link_eci_packet_rx.dcs_c5_wd_pkt_ready
);

-- TX serializer.
-- Serialize ECI packet into eci_gateway.
-- TX rsp_wd VC5.
i_dcs_c5_bus_to_eci_channel : eci_bus_channel_converter
port map (
    clk             => clk,
    -- Input ECI packet.
    in_data         => vector_to_words(link_eci_packet_tx.dcs_c5_wd_pkt),
    in_vc_no        => link_eci_packet_tx.dcs_c5_wd_pkt_vc,
    in_size         => link_eci_packet_tx.dcs_c5_wd_pkt_size,
    in_valid        => link_eci_packet_tx.dcs_c5_wd_pkt_valid,
    in_ready        => link_eci_packet_tx.dcs_c5_wd_pkt_ready,
    -- output eci_gateway packet.
    out_channel     => link_eci_packet_tx.dcs_c5,
    out_ready       => link_eci_packet_tx.dcs_c5_ready
);

-- RX packetizer.
-- Packetize data from eci_gateway into ECI packet. 
-- RX rsp_wd VC4.
i_dcs_c4_eci_channel_to_bus : eci_channel_bus_converter
port map (
    clk             => clk,
    -- Input eci_gateway packet.
    in_channel      => link_eci_packet_rx.dcs_c4,
    in_ready        => link_eci_packet_rx.dcs_c4_ready,
    -- output ECI packet.
    out_data        => link_eci_packet_rx.dcs_c4_wd_pkt,
    out_vc_no       => link_eci_packet_rx.dcs_c4_wd_pkt_vc,
    out_size        => link_eci_packet_rx.dcs_c4_wd_pkt_size,
    out_valid       => link_eci_packet_rx.dcs_c4_wd_pkt_valid,
    out_ready       => link_eci_packet_rx.dcs_c4_wd_pkt_ready
);

-- TX serializer.
-- Serialize ECI packet into eci_gateway.
-- TX rsp_wd VC4.
i_dcs_c4_bus_to_eci_channel : eci_bus_channel_converter
port map (
    clk             => clk,
    -- Input ECI packet.
    in_data         => vector_to_words(link_eci_packet_tx.dcs_c4_wd_pkt),
    in_vc_no        => link_eci_packet_tx.dcs_c4_wd_pkt_vc,
    in_size         => link_eci_packet_tx.dcs_c4_wd_pkt_size,
    in_valid        => link_eci_packet_tx.dcs_c4_wd_pkt_valid,
    in_ready        => link_eci_packet_tx.dcs_c4_wd_pkt_ready,
    -- output eci_gateway packet.
    out_channel     => link_eci_packet_tx.dcs_c4,
    out_ready       => link_eci_packet_tx.dcs_c4_ready
);

-- DC Slices: One DCS for odd and another for even VCs.
-- DCS for even VCs ie odd CL indices. 
dcs_even : dcs_2_axi
port map (
  clk   => clk,
  reset => reset,

  -- Input ECI events.
  -- ECI packet for request without data. (VC 6 or 7) (only header).
  req_wod_hdr_i       => link_eci_packet_rx.dcs_c6.data(0),
  req_wod_pkt_size_i  => "00001",
  req_wod_pkt_vc_i    => link_eci_packet_rx.dcs_c6.vc_no,
  req_wod_pkt_valid_i => link_eci_packet_rx.dcs_c6.valid,
  req_wod_pkt_ready_o => link_eci_packet_rx.dcs_c6_ready,

  -- ECI packet for response without data.(VC 10 or 11). (only header).
  rsp_wod_hdr_i       => link_eci_packet_rx.dcs_c10.data(0),
  rsp_wod_pkt_size_i  => "00001",
  rsp_wod_pkt_vc_i    => link_eci_packet_rx.dcs_c10.vc_no,
  rsp_wod_pkt_valid_i => link_eci_packet_rx.dcs_c10.valid,
  rsp_wod_pkt_ready_o => link_eci_packet_rx.dcs_c10_ready,

  -- ECI packet for response with data. (VC 4 or 5). (header + data).
  rsp_wd_pkt_i        => words_to_vector(link_eci_packet_rx.dcs_c4_wd_pkt),
  rsp_wd_pkt_size_i   => link_eci_packet_rx.dcs_c4_wd_pkt_size,
  rsp_wd_pkt_vc_i     => link_eci_packet_rx.dcs_c4_wd_pkt_vc,
  rsp_wd_pkt_valid_i  => link_eci_packet_rx.dcs_c4_wd_pkt_valid,
  rsp_wd_pkt_ready_o  => link_eci_packet_rx.dcs_c4_wd_pkt_ready,

  -- ECI packet for lcl fwd without data (VC 16 or 17).
  lcl_fwd_wod_hdr_i       => dcs_c16_i.data,
  lcl_fwd_wod_pkt_size_i  => dcs_c16_i.size,
  lcl_fwd_wod_pkt_vc_i    => dcs_c16_i.vc_no, --5 bits not 4.
  lcl_fwd_wod_pkt_valid_i => dcs_c16_i.valid,
  lcl_fwd_wod_pkt_ready_o => dcs_c16_i.ready,

  -- ECI packet for lcl rsp without data (VC 18 or 19).
  lcl_rsp_wod_hdr_i       => dcs_c18_i.data,
  lcl_rsp_wod_pkt_size_i  => dcs_c18_i.size,
  lcl_rsp_wod_pkt_vc_i    => dcs_c18_i.vc_no, --5 bits not 4.
  lcl_rsp_wod_pkt_valid_i => dcs_c18_i.valid,
  lcl_rsp_wod_pkt_ready_o => dcs_c18_i.ready,
  
  -- Output ECI events. (rsp without data, rsp with data).
  -- VC 10,11
  rsp_wod_hdr_o                  => link_eci_packet_tx.dcs_c10.data(0),
  rsp_wod_pkt_size_o             => open,
  rsp_wod_pkt_vc_o               => link_eci_packet_tx.dcs_c10.vc_no,
  rsp_wod_pkt_valid_o            => link_eci_packet_tx.dcs_c10.valid,
  rsp_wod_pkt_ready_i            => link_eci_packet_tx.dcs_c10_ready,

  -- Responses with data (VC 5 or 4)
  -- header+payload
  rsp_wd_pkt_o                   => link_eci_packet_tx.dcs_c4_wd_pkt,
  rsp_wd_pkt_size_o              => link_eci_packet_tx.dcs_c4_wd_pkt_size,
  rsp_wd_pkt_vc_o                => link_eci_packet_tx.dcs_c4_wd_pkt_vc,
  rsp_wd_pkt_valid_o             => link_eci_packet_tx.dcs_c4_wd_pkt_valid,
  rsp_wd_pkt_ready_i             => link_eci_packet_tx.dcs_c4_wd_pkt_ready,

  -- ECI fwd without data (VC 8 or 9)
  fwd_wod_hdr_o       => link_eci_packet_tx.dcs_c8.data(0), 
  fwd_wod_pkt_size_o  => open,
  fwd_wod_pkt_vc_o    => link_eci_packet_tx.dcs_c8.vc_no,
  fwd_wod_pkt_valid_o => link_eci_packet_tx.dcs_c8.valid,
  fwd_wod_pkt_ready_i => link_eci_packet_tx.dcs_c8_ready,

  -- Lcl rsp without data (VC 18 or 19)
  lcl_rsp_wod_hdr_o       => dcs_c18_o.data,
  lcl_rsp_wod_pkt_size_o  => dcs_c18_o.size,
  lcl_rsp_wod_pkt_vc_o    => dcs_c18_o.vc_no, --5 bits not 4.
  lcl_rsp_wod_pkt_valid_o => dcs_c18_o.valid,
  lcl_rsp_wod_pkt_ready_i => dcs_c18_o.ready,
  
  -- Primary AXI rd/wr i/f.
  p_axi_arid    => dcs_even_axi.arid,
  p_axi_araddr  => dcs_even_axi.araddr,
  p_axi_arlen   => dcs_even_axi.arlen,
  p_axi_arsize  => dcs_even_axi.arsize,
  p_axi_arburst => dcs_even_axi.arburst,
  p_axi_arlock  => dcs_even_axi.arlock,
  p_axi_arcache => dcs_even_axi.arcache,
  p_axi_arprot  => dcs_even_axi.arprot,
  p_axi_arvalid => dcs_even_axi.arvalid,
  p_axi_arready => dcs_even_axi.arready,
  p_axi_rid     => dcs_even_axi.rid,
  p_axi_rdata   => dcs_even_axi.rdata,
  p_axi_rresp   => dcs_even_axi.rresp,
  p_axi_rlast   => dcs_even_axi.rlast,
  p_axi_rvalid  => dcs_even_axi.rvalid,
  p_axi_rready  => dcs_even_axi.rready,

  p_axi_awid    => dcs_even_axi.awid,
  p_axi_awaddr  => dcs_even_axi.awaddr,
  p_axi_awlen   => dcs_even_axi.awlen,
  p_axi_awsize  => dcs_even_axi.awsize,
  p_axi_awburst => dcs_even_axi.awburst,
  p_axi_awlock  => dcs_even_axi.awlock,
  p_axi_awcache => dcs_even_axi.awcache,
  p_axi_awprot  => dcs_even_axi.awprot,
  p_axi_awvalid => dcs_even_axi.awvalid,
  p_axi_awready => dcs_even_axi.awready,
  p_axi_wdata   => dcs_even_axi.wdata,
  p_axi_wstrb   => dcs_even_axi.wstrb,
  p_axi_wlast   => dcs_even_axi.wlast,
  p_axi_wvalid  => dcs_even_axi.wvalid,
  p_axi_wready  => dcs_even_axi.wready,
  p_axi_bid     => dcs_even_axi.bid,
  p_axi_bresp   => dcs_even_axi.bresp,
  p_axi_bvalid  => dcs_even_axi.bvalid,
  p_axi_bready  => dcs_even_axi.bready,
  
  tracing_valid     => dc_tracing_valid(1 downto 0),
  tracing_error     => dc_tracing_error(1 downto 0),
  tracing_cli       => dc_tracing_cli(1 downto 0),
  tracing_state     => dc_tracing_state(1 downto 0),
  tracing_action    => dc_tracing_action(1 downto 0),
  tracing_request   => dc_tracing_request(1 downto 0)
);

-- DCS for odd VCs ie even CL indices. 
dcs_odd : dcs_2_axi
port map (
  clk   => clk,
  reset => reset,

  -- Input ECI events.
  -- ECI packet for request without data. (VC 6 or 7) (only header).
  req_wod_hdr_i       => link_eci_packet_rx.dcs_c7.data(0),
  req_wod_pkt_size_i  => "00001",
  req_wod_pkt_vc_i    => link_eci_packet_rx.dcs_c7.vc_no,
  req_wod_pkt_valid_i => link_eci_packet_rx.dcs_c7.valid,
  req_wod_pkt_ready_o => link_eci_packet_rx.dcs_c7_ready,

  -- ECI packet for response without data.(VC 10 or 11). (only header).
  rsp_wod_hdr_i       => link_eci_packet_rx.dcs_c11.data(0),
  rsp_wod_pkt_size_i  => "00001",
  rsp_wod_pkt_vc_i    => link_eci_packet_rx.dcs_c11.vc_no,
  rsp_wod_pkt_valid_i => link_eci_packet_rx.dcs_c11.valid,
  rsp_wod_pkt_ready_o => link_eci_packet_rx.dcs_c11_ready,

  -- ECI packet for response with data. (VC 4 or 5). (header + data).
  rsp_wd_pkt_i        => words_to_vector(link_eci_packet_rx.dcs_c5_wd_pkt),
  rsp_wd_pkt_size_i   => link_eci_packet_rx.dcs_c5_wd_pkt_size,
  rsp_wd_pkt_vc_i     => link_eci_packet_rx.dcs_c5_wd_pkt_vc,
  rsp_wd_pkt_valid_i  => link_eci_packet_rx.dcs_c5_wd_pkt_valid,
  rsp_wd_pkt_ready_o  => link_eci_packet_rx.dcs_c5_wd_pkt_ready,

  -- ECI packet for lcl fwd without data (VC 16 or 17).
  lcl_fwd_wod_hdr_i       => dcs_c17_i.data,
  lcl_fwd_wod_pkt_size_i  => dcs_c17_i.size,
  lcl_fwd_wod_pkt_vc_i    => dcs_c17_i.vc_no, --5 bits not 4.
  lcl_fwd_wod_pkt_valid_i => dcs_c17_i.valid,
  lcl_fwd_wod_pkt_ready_o => dcs_c17_i.ready,

  -- ECI packet for lcl rsp without data (VC 18 or 19).
  lcl_rsp_wod_hdr_i       => dcs_c19_i.data,
  lcl_rsp_wod_pkt_size_i  => dcs_c19_i.size,
  lcl_rsp_wod_pkt_vc_i    => dcs_c19_i.vc_no,
  lcl_rsp_wod_pkt_valid_i => dcs_c19_i.valid,
  lcl_rsp_wod_pkt_ready_o => dcs_c19_i.ready,

  -- Output ECI events. (rsp without data, rsp with data).
  -- VC 10,11
  rsp_wod_hdr_o                  => link_eci_packet_tx.dcs_c11.data(0),
  rsp_wod_pkt_size_o             => open,
  rsp_wod_pkt_vc_o               => link_eci_packet_tx.dcs_c11.vc_no,
  rsp_wod_pkt_valid_o            => link_eci_packet_tx.dcs_c11.valid,
  rsp_wod_pkt_ready_i            => link_eci_packet_tx.dcs_c11_ready,

  -- Responses with data (VC 5 or 4)
  -- header+payload
  rsp_wd_pkt_o                   => link_eci_packet_tx.dcs_c5_wd_pkt,
  rsp_wd_pkt_size_o              => link_eci_packet_tx.dcs_c5_wd_pkt_size,
  rsp_wd_pkt_vc_o                => link_eci_packet_tx.dcs_c5_wd_pkt_vc,
  rsp_wd_pkt_valid_o             => link_eci_packet_tx.dcs_c5_wd_pkt_valid,
  rsp_wd_pkt_ready_i             => link_eci_packet_tx.dcs_c5_wd_pkt_ready,

  -- ECI fwd without data (VC 8 or 9)
  fwd_wod_hdr_o       => link_eci_packet_tx.dcs_c9.data(0), 
  fwd_wod_pkt_size_o  => open,
  fwd_wod_pkt_vc_o    => link_eci_packet_tx.dcs_c9.vc_no,
  fwd_wod_pkt_valid_o => link_eci_packet_tx.dcs_c9.valid,
  fwd_wod_pkt_ready_i => link_eci_packet_tx.dcs_c9_ready,

  -- Lcl rsp without data (VC 18 or 19)
  lcl_rsp_wod_hdr_o       => dcs_c19_o.data,
  lcl_rsp_wod_pkt_size_o  => dcs_c19_o.size,
  lcl_rsp_wod_pkt_vc_o    => dcs_c19_o.vc_no, --5 bits not 4.
  lcl_rsp_wod_pkt_valid_o => dcs_c19_o.valid,
  lcl_rsp_wod_pkt_ready_i => dcs_c19_o.ready,

  -- Primary AXI rd/wr i/f.
  p_axi_arid    => dcs_odd_axi.arid,
  p_axi_araddr  => dcs_odd_axi.araddr,
  p_axi_arlen   => dcs_odd_axi.arlen,
  p_axi_arsize  => dcs_odd_axi.arsize,
  p_axi_arburst => dcs_odd_axi.arburst,
  p_axi_arlock  => dcs_odd_axi.arlock,
  p_axi_arcache => dcs_odd_axi.arcache,
  p_axi_arprot  => dcs_odd_axi.arprot,
  p_axi_arvalid => dcs_odd_axi.arvalid,
  p_axi_arready => dcs_odd_axi.arready,
  p_axi_rid     => dcs_odd_axi.rid,
  p_axi_rdata   => dcs_odd_axi.rdata,
  p_axi_rresp   => dcs_odd_axi.rresp,
  p_axi_rlast   => dcs_odd_axi.rlast,
  p_axi_rvalid  => dcs_odd_axi.rvalid,
  p_axi_rready  => dcs_odd_axi.rready,

  p_axi_awid    => dcs_odd_axi.awid,
  p_axi_awaddr  => dcs_odd_axi.awaddr,
  p_axi_awlen   => dcs_odd_axi.awlen,
  p_axi_awsize  => dcs_odd_axi.awsize,
  p_axi_awburst => dcs_odd_axi.awburst,
  p_axi_awlock  => dcs_odd_axi.awlock,
  p_axi_awcache => dcs_odd_axi.awcache,
  p_axi_awprot  => dcs_odd_axi.awprot,
  p_axi_awvalid => dcs_odd_axi.awvalid,
  p_axi_awready => dcs_odd_axi.awready,
  p_axi_wdata   => dcs_odd_axi.wdata,
  p_axi_wstrb   => dcs_odd_axi.wstrb,
  p_axi_wlast   => dcs_odd_axi.wlast,
  p_axi_wvalid  => dcs_odd_axi.wvalid,
  p_axi_wready  => dcs_odd_axi.wready,
  p_axi_bid     => dcs_odd_axi.bid,
  p_axi_bresp   => dcs_odd_axi.bresp,
  p_axi_bvalid  => dcs_odd_axi.bvalid,
  p_axi_bready  => dcs_odd_axi.bready,

  tracing_valid     => dc_tracing_valid(3 downto 2),
  tracing_error     => dc_tracing_error(3 downto 2),
  tracing_cli       => dc_tracing_cli(3 downto 2),
  tracing_state     => dc_tracing_state(3 downto 2),
  tracing_action    => dc_tracing_action(3 downto 2),
  tracing_request   => dc_tracing_request(3 downto 2)
);

G_DDR : if DDR_OR_BRAM = 1 generate
-- Connect AXI interfaces from DCS to DDR.
i_dcs_ddr: design_1
  port map(
    clk_sys => clk,
    reset_n => reset_n,
    ddr4_sys_rst => reset,
    -- DCS EVEN AXI
    ddr4_c1_axi_arid => dcs_even_axi.arid,
    ddr4_c1_axi_araddr => shift_address(dcs_even_axi.araddr),
    ddr4_c1_axi_arlen => dcs_even_axi.arlen,
    ddr4_c1_axi_arsize => dcs_even_axi.arsize,
    ddr4_c1_axi_arburst => dcs_even_axi.arburst,
    ddr4_c1_axi_arlock(0) => dcs_even_axi.arlock,
    ddr4_c1_axi_arcache => dcs_even_axi.arcache,
    ddr4_c1_axi_arprot => dcs_even_axi.arprot,
    ddr4_c1_axi_arvalid => dcs_even_axi.arvalid,
    ddr4_c1_axi_arready => dcs_even_axi.arready,
    ddr4_c1_axi_arqos => (others => '0'),
    ddr4_c1_axi_arregion => (others => '0'),
    ddr4_c1_axi_rid => dcs_even_axi.rid,
    ddr4_c1_axi_rdata => dcs_even_axi.rdata,
    ddr4_c1_axi_rresp => dcs_even_axi.rresp,
    ddr4_c1_axi_rlast => dcs_even_axi.rlast,
    ddr4_c1_axi_rvalid => dcs_even_axi.rvalid,
    ddr4_c1_axi_rready => dcs_even_axi.rready,
    ddr4_c1_axi_awid => dcs_even_axi.awid,
    ddr4_c1_axi_awaddr => shift_address(dcs_even_axi.awaddr),
    ddr4_c1_axi_awlen => dcs_even_axi.awlen,
    ddr4_c1_axi_awsize => dcs_even_axi.awsize,
    ddr4_c1_axi_awburst => dcs_even_axi.awburst,
    ddr4_c1_axi_awlock(0) => dcs_even_axi.awlock,
    ddr4_c1_axi_awcache => dcs_even_axi.awcache,
    ddr4_c1_axi_awprot => dcs_even_axi.awprot,
    ddr4_c1_axi_awvalid => dcs_even_axi.awvalid,
    ddr4_c1_axi_awready => dcs_even_axi.awready,
    ddr4_c1_axi_awqos => (others => '0'),
    ddr4_c1_axi_awregion => (others => '0'),
    ddr4_c1_axi_wdata => dcs_even_axi.wdata,
    ddr4_c1_axi_wstrb => dcs_even_axi.wstrb,
    ddr4_c1_axi_wlast => dcs_even_axi.wlast,
    ddr4_c1_axi_wvalid => dcs_even_axi.wvalid,
    ddr4_c1_axi_wready => dcs_even_axi.wready,
    ddr4_c1_axi_bid => dcs_even_axi.bid,
    ddr4_c1_axi_bresp => dcs_even_axi.bresp,
    ddr4_c1_axi_bvalid => dcs_even_axi.bvalid,
    ddr4_c1_axi_bready => dcs_even_axi.bready,

    -- DCS ODD AXI
    ddr4_c4_axi_arid => dcs_odd_axi.arid,
    ddr4_c4_axi_araddr => shift_address(dcs_odd_axi.araddr),
    ddr4_c4_axi_arlen => dcs_odd_axi.arlen,
    ddr4_c4_axi_arsize => dcs_odd_axi.arsize,
    ddr4_c4_axi_arburst => dcs_odd_axi.arburst,
    ddr4_c4_axi_arlock(0) => dcs_odd_axi.arlock,
    ddr4_c4_axi_arcache => dcs_odd_axi.arcache,
    ddr4_c4_axi_arprot => dcs_odd_axi.arprot,
    ddr4_c4_axi_arvalid => dcs_odd_axi.arvalid,
    ddr4_c4_axi_arready => dcs_odd_axi.arready,
    ddr4_c4_axi_arqos => (others => '0'),
    ddr4_c4_axi_arregion => (others => '0'),
    ddr4_c4_axi_rid => dcs_odd_axi.rid,
    ddr4_c4_axi_rdata => dcs_odd_axi.rdata,
    ddr4_c4_axi_rresp => dcs_odd_axi.rresp,
    ddr4_c4_axi_rlast => dcs_odd_axi.rlast,
    ddr4_c4_axi_rvalid => dcs_odd_axi.rvalid,
    ddr4_c4_axi_rready => dcs_odd_axi.rready,
    ddr4_c4_axi_awid => dcs_odd_axi.awid,
    ddr4_c4_axi_awaddr => shift_address(dcs_odd_axi.awaddr),
    ddr4_c4_axi_awlen => dcs_odd_axi.awlen,
    ddr4_c4_axi_awsize => dcs_odd_axi.awsize,
    ddr4_c4_axi_awburst => dcs_odd_axi.awburst,
    ddr4_c4_axi_awlock(0) => dcs_odd_axi.awlock,
    ddr4_c4_axi_awcache => dcs_odd_axi.awcache,
    ddr4_c4_axi_awprot => dcs_odd_axi.awprot,
    ddr4_c4_axi_awvalid => dcs_odd_axi.awvalid,
    ddr4_c4_axi_awready => dcs_odd_axi.awready,
    ddr4_c4_axi_awqos => (others => '0'),
    ddr4_c4_axi_awregion => (others => '0'),
    ddr4_c4_axi_wdata => dcs_odd_axi.wdata,
    ddr4_c4_axi_wstrb => dcs_odd_axi.wstrb,
    ddr4_c4_axi_wlast => dcs_odd_axi.wlast,
    ddr4_c4_axi_wvalid => dcs_odd_axi.wvalid,
    ddr4_c4_axi_wready => dcs_odd_axi.wready,
    ddr4_c4_axi_bid => dcs_odd_axi.bid,
    ddr4_c4_axi_bresp => dcs_odd_axi.bresp,
    ddr4_c4_axi_bvalid => dcs_odd_axi.bvalid,
    ddr4_c4_axi_bready => dcs_odd_axi.bready,

    -- C1 axil ctrl not connected.
    ddr4_c1_axi_ctrl_araddr => (others => '0'),
    ddr4_c1_axi_ctrl_arready => open,
    ddr4_c1_axi_ctrl_arvalid => '0',
    ddr4_c1_axi_ctrl_awaddr => (others => '0'),
    ddr4_c1_axi_ctrl_awready => open,
    ddr4_c1_axi_ctrl_awvalid => '0',
    ddr4_c1_axi_ctrl_bready => '0',
    ddr4_c1_axi_ctrl_bresp => open,
    ddr4_c1_axi_ctrl_bvalid => open,
    ddr4_c1_axi_ctrl_rdata => open,
    ddr4_c1_axi_ctrl_rready => '0',
    ddr4_c1_axi_ctrl_rresp => open,
    ddr4_c1_axi_ctrl_rvalid => open,
    ddr4_c1_axi_ctrl_wdata => (others => '0'),
    ddr4_c1_axi_ctrl_wready => open,
    ddr4_c1_axi_ctrl_wvalid => '0',

    -- C4 axil ctrl not connected.
    ddr4_c4_axi_ctrl_araddr => (others => '0'),
    ddr4_c4_axi_ctrl_arready => open,
    ddr4_c4_axi_ctrl_arvalid => '0',
    ddr4_c4_axi_ctrl_awaddr => (others => '0'),
    ddr4_c4_axi_ctrl_awready => open,
    ddr4_c4_axi_ctrl_awvalid => '0',
    ddr4_c4_axi_ctrl_bready => '0',
    ddr4_c4_axi_ctrl_bresp => open,
    ddr4_c4_axi_ctrl_bvalid => open,
    ddr4_c4_axi_ctrl_rdata => open,
    ddr4_c4_axi_ctrl_rready => '0',
    ddr4_c4_axi_ctrl_rresp => open,
    ddr4_c4_axi_ctrl_rvalid => open,
    ddr4_c4_axi_ctrl_wdata => (others => '0'),
    ddr4_c4_axi_ctrl_wready => open,
    ddr4_c4_axi_ctrl_wvalid => '0',

    ddr4_c1_act_n     => F_D1_ACT_N,
    ddr4_c1_adr       => F_D1_A(16 downto 0),
    ddr4_c1_ba        => F_D1_BA,
    ddr4_c1_bg        => F_D1_BG,
    ddr4_c1_ck_c(0)   => F_D1_CK_N(0),
    ddr4_c1_ck_t(0)   => F_D1_CK_P(0),
    ddr4_c1_cke(0)    => F_D1_CKE(0),
    ddr4_c1_cs_n(0)   => F_D1_CS_N(0),
    ddr4_c1_dq        => F_D1_DQ,
    ddr4_c1_dqs_c     => F_D1_DQS_N,
    ddr4_c1_dqs_t     => F_D1_DQS_P,
    ddr4_c1_odt(0)    => F_D1_ODT(0),
    ddr4_c1_par       => F_D1_PARITY_N,
    ddr4_c1_reset_n   => F_D1_RESET_N,
    ddr4_c1_sys_clk_n => F_D1C_CLK_N,
    ddr4_c1_sys_clk_p => F_D1C_CLK_P,
    ddr4_c1_ui_clk    => open,

    ddr4_c4_act_n     => F_D4_ACT_N,
    ddr4_c4_adr       => F_D4_A(16 downto 0),
    ddr4_c4_ba        => F_D4_BA,
    ddr4_c4_bg        => F_D4_BG,
    ddr4_c4_ck_c(0)   => F_D4_CK_N(0),
    ddr4_c4_ck_t(0)   => F_D4_CK_P(0),
    ddr4_c4_cke(0)    => F_D4_CKE(0),
    ddr4_c4_cs_n(0)   => F_D4_CS_N(0),
    ddr4_c4_dq        => F_D4_DQ,
    ddr4_c4_dqs_c     => F_D4_DQS_N,
    ddr4_c4_dqs_t     => F_D4_DQS_P,
    ddr4_c4_odt(0)    => F_D4_ODT(0),
    ddr4_c4_par       => F_D4_PARITY_N,
    ddr4_c4_reset_n   => F_D4_RESET_N,
    ddr4_c4_sys_clk_n => F_D4C_CLK_N,
    ddr4_c4_sys_clk_p => F_D4C_CLK_P,
    ddr4_c4_ui_clk    => open
    );
end generate G_DDR;

G_BRAM : if DDR_OR_BRAM = 0 generate
-- Connect AXI interfaces from each DC slice instance to a BRAM.
-- BRAM for even DCS io odd CL indices.
-- Only lower 16 bits of the address are connected.
i_even_dcs_bram: axi_bram_ctrl_0
port map(
    s_axi_aclk    => clk,
    s_axi_aresetn => reset_n,
    s_axi_awid    => dcs_even_axi.awid,
    s_axi_awaddr  => shift_address(dcs_even_axi.awaddr)(15 downto 0),
    s_axi_awlen   => dcs_even_axi.awlen,
    s_axi_awsize  => dcs_even_axi.awsize,
    s_axi_awburst => dcs_even_axi.awburst,
    s_axi_awlock  => dcs_even_axi.awlock,
    s_axi_awcache => dcs_even_axi.awcache,
    s_axi_awprot  => dcs_even_axi.awprot,
    s_axi_awvalid => dcs_even_axi.awvalid,
    s_axi_awready => dcs_even_axi.awready,
    s_axi_wdata   => dcs_even_axi.wdata,
    s_axi_wstrb   => dcs_even_axi.wstrb,
    s_axi_wlast   => dcs_even_axi.wlast,
    s_axi_wvalid  => dcs_even_axi.wvalid,
    s_axi_wready  => dcs_even_axi.wready,
    s_axi_bid     => dcs_even_axi.bid,
    s_axi_bresp   => dcs_even_axi.bresp,
    s_axi_bvalid  => dcs_even_axi.bvalid,
    s_axi_bready  => dcs_even_axi.bready,
    s_axi_arid    => dcs_even_axi.arid,
    s_axi_araddr  => shift_address(dcs_even_axi.araddr)(15 downto 0),
    s_axi_arlen   => dcs_even_axi.arlen,
    s_axi_arsize  => dcs_even_axi.arsize,
    s_axi_arburst => dcs_even_axi.arburst,
    s_axi_arlock  => dcs_even_axi.arlock,
    s_axi_arcache => dcs_even_axi.arcache,
    s_axi_arprot  => dcs_even_axi.arprot,
    s_axi_arvalid => dcs_even_axi.arvalid,
    s_axi_arready => dcs_even_axi.arready,
    s_axi_rid     => dcs_even_axi.rid,
    s_axi_rdata   => dcs_even_axi.rdata,
    s_axi_rresp   => dcs_even_axi.rresp,
    s_axi_rlast   => dcs_even_axi.rlast,
    s_axi_rvalid  => dcs_even_axi.rvalid,
    s_axi_rready  => dcs_even_axi.rready
);

-- -- BRAM for odd DCS io even CL indices.
-- -- Only lower 16 bits of the address are connected.
i_odd_dcs_bram: axi_bram_ctrl_0
port map(
    s_axi_aclk    => clk,
    s_axi_aresetn => reset_n,
    s_axi_awid    => dcs_odd_axi.awid,
    s_axi_awaddr  => shift_address(dcs_odd_axi.awaddr)(15 downto 0),
    s_axi_awlen   => dcs_odd_axi.awlen,
    s_axi_awsize  => dcs_odd_axi.awsize,
    s_axi_awburst => dcs_odd_axi.awburst,
    s_axi_awlock  => dcs_odd_axi.awlock,
    s_axi_awcache => dcs_odd_axi.awcache,
    s_axi_awprot  => dcs_odd_axi.awprot,
    s_axi_awvalid => dcs_odd_axi.awvalid,
    s_axi_awready => dcs_odd_axi.awready,
    s_axi_wdata   => dcs_odd_axi.wdata,
    s_axi_wstrb   => dcs_odd_axi.wstrb,
    s_axi_wlast   => dcs_odd_axi.wlast,
    s_axi_wvalid  => dcs_odd_axi.wvalid,
    s_axi_wready  => dcs_odd_axi.wready,
    s_axi_bid     => dcs_odd_axi.bid,
    s_axi_bresp   => dcs_odd_axi.bresp,
    s_axi_bvalid  => dcs_odd_axi.bvalid,
    s_axi_bready  => dcs_odd_axi.bready,
    s_axi_arid    => dcs_odd_axi.arid,
    s_axi_araddr  => shift_address(dcs_odd_axi.araddr)(15 downto 0),
    s_axi_arlen   => dcs_odd_axi.arlen,
    s_axi_arsize  => dcs_odd_axi.arsize,
    s_axi_arburst => dcs_odd_axi.arburst,
    s_axi_arlock  => dcs_odd_axi.arlock,
    s_axi_arcache => dcs_odd_axi.arcache,
    s_axi_arprot  => dcs_odd_axi.arprot,
    s_axi_arvalid => dcs_odd_axi.arvalid,
    s_axi_arready => dcs_odd_axi.arready,
    s_axi_rid     => dcs_odd_axi.rid,
    s_axi_rdata   => dcs_odd_axi.rdata,
    s_axi_rresp   => dcs_odd_axi.rresp,
    s_axi_rlast   => dcs_odd_axi.rlast,
    s_axi_rvalid  => dcs_odd_axi.rvalid,
    s_axi_rready  => dcs_odd_axi.rready
);
end generate G_BRAM;

dcs_even_ar_hs <= dcs_even_axi.arvalid and dcs_even_axi.arready;
dcs_odd_ar_hs <= dcs_odd_axi.arvalid and dcs_odd_axi.arready;        
G_LCI_PERF_1 : if LCI_PERF_GEN = 1 generate
-- Generate seq aliased addr to sweep an address range.
-- odd means odd cl indices, should be connected to even dcs.
-- and vice versa.
lci_perf_1: lci_perf
  generic map(
    LCI_LAT_TPUT => LCI_LAT_TPUT,
    NUM_REQS_TO_WAIT => 65536,
    NUM_ADDR_TO_ISSUE => 65536,
    PERF_REGS_WIDTH => 32,
    FIFO_DEPTH => 512
    )
  port map(
    clk => clk,
    reset => reset,
    odd_wait_req_valid_i  => dcs_even_ar_hs,
    even_wait_req_valid_i => dcs_odd_ar_hs,
    -- outgoing LCI for odd cl index to VC 16.
    lci_odd_idx_hdr_o   => dcs_c16_i.data,
    lci_odd_idx_size_o  => dcs_c16_i.size,
    lci_odd_idx_vc_o    => dcs_c16_i.vc_no,
    lci_odd_idx_valid_o => dcs_c16_i.valid,
    lci_odd_idx_ready_i => dcs_c16_i.ready,
    -- Incoming LCIA for odd cl index from VC 18.
    lcia_odd_idx_hdr_i       => dcs_c18_o.data,
    lcia_odd_idx_size_i  => dcs_c18_o.size,
    lcia_odd_idx_vc_i    => dcs_c18_o.vc_no,
    lcia_odd_idx_valid_i => dcs_c18_o.valid,
    lcia_odd_idx_ready_o => dcs_c18_o.ready,
    -- Outgoing unlock for odd cl index to VC 18.
    ul_odd_idx_hdr_o       => dcs_c18_i.data,
    ul_odd_idx_size_o  => dcs_c18_i.size,
    ul_odd_idx_vc_o    => dcs_c18_i.vc_no,
    ul_odd_idx_valid_o => dcs_c18_i.valid,
    ul_odd_idx_ready_i => dcs_c18_i.ready,
    -- outgoing LCI for even cl index to even VC 17.
    lci_even_idx_hdr_o   => dcs_c17_i.data,
    lci_even_idx_size_o  => dcs_c17_i.size,
    lci_even_idx_vc_o    => dcs_c17_i.vc_no,
    lci_even_idx_valid_o => dcs_c17_i.valid,
    lci_even_idx_ready_i => dcs_c17_i.ready,
    -- Incoming LCIA for even cl index from VC 19.
    lcia_even_idx_hdr_i       => dcs_c19_o.data,
    lcia_even_idx_size_i  => dcs_c19_o.size,
    lcia_even_idx_vc_i    => dcs_c19_o.vc_no,
    lcia_even_idx_valid_i => dcs_c19_o.valid,
    lcia_even_idx_ready_o => dcs_c19_o.ready,
    -- Outgoing unlock for even cl index to VC 19.
    ul_even_idx_hdr_o       => dcs_c19_i.data,
    ul_even_idx_size_o  => dcs_c19_i.size,
    ul_even_idx_vc_o    => dcs_c19_i.vc_no,
    ul_even_idx_valid_o => dcs_c19_i.valid,
    ul_even_idx_ready_i => dcs_c19_i.ready,
    done_o => open
);
end generate G_LCI_PERF_1;

G_LCI_PERF_0 : if LCI_PERF_GEN = 0 generate
  dcs_c16_i.data <= (others => '0');
  dcs_c16_i.size <= (others => '0');
  dcs_c16_i.vc_no <= (others => '0');
  dcs_c16_i.valid <= '0';
  dcs_c18_o.ready <= '0';
  dcs_c18_i.data <= (others => '0');
  dcs_c18_i.size <= (others => '0');
  dcs_c18_i.vc_no <= (others => '0');
  dcs_c18_i.valid <= '0';
  dcs_c17_i.data <= (others => '0');
  dcs_c17_i.size <= (others => '0');
  dcs_c17_i.vc_no <= (others => '0');
  dcs_c17_i.valid <= '0';
  dcs_c19_o.ready <= '0';
  dcs_c19_i.data <= (others => '0');
  dcs_c19_i.size <= (others => '0');
  dcs_c19_i.vc_no <= (others => '0');
  dcs_c19_i.valid <= '0';
end generate G_LCI_PERF_0;

i_ila_dc_trace : entity work.ila_dc_trace
port map (
    clk => clk,
    probe0(0)   => dc_tracing_valid(0),
    probe1(0)   => dc_tracing_error(0),
    probe2      => dc_tracing_cli(0),
    probe3      => dc_tracing_state(0),
    probe4      => dc_tracing_action(0),
    probe5      => dc_tracing_request(0),
    probe6(0)   => dc_tracing_valid(1),
    probe7(0)   => dc_tracing_error(1),
    probe8      => dc_tracing_cli(1),
    probe9      => dc_tracing_state(1),
    probe10     => dc_tracing_action(1),
    probe11     => dc_tracing_request(1),
    probe12(0)  => dc_tracing_valid(2),
    probe13(0)  => dc_tracing_error(2),
    probe14     => dc_tracing_cli(2),
    probe15     => dc_tracing_state(2),
    probe16     => dc_tracing_action(2),
    probe17     => dc_tracing_request(2),
    probe18(0)  => dc_tracing_valid(3),
    probe19(0)  => dc_tracing_error(3),
    probe20     => dc_tracing_cli(3),
    probe21     => dc_tracing_state(3),
    probe22     => dc_tracing_action(3),
    probe23     => dc_tracing_request(3)
);

end Behavioral;
